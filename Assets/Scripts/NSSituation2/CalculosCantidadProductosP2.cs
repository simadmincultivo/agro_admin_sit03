﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CalculosCantidadProductosP2 : MonoBehaviour
{
    [Header("Variable calificación")]
    public float calificacion1;

    [Header("Referencia a Scripts")]
    [SerializeField] private ValuesProductosManagerP2 myValuesProductosManagerP2;

    [Header("Cantidades de productos y fields de fertilización")]
    [SerializeField] private float cantidadAbono1;
    [SerializeField] private float cantidadAbono2;
    [SerializeField] private float cantidadAbono3;
    [SerializeField] private float cantidadMateriaOrganica;
    [SerializeField] private float cantidadPorPlantaAbono1;
    [SerializeField] private float cantidadPorPlantaAbono2;
    [SerializeField] private float cantidadPorPlantaAbono3;
    [SerializeField] private float cantidadPorPlantaMateria;
    [SerializeField] private TMP_InputField[] fieldsFertilizacion;

    [Header("Cantidades de productos de plagas y enfermedades")]
    [System.NonSerialized] private float cantidadInsecticida1;
    [System.NonSerialized] private float cantidadInsecticida2;
    [System.NonSerialized] private float cantidadFungicida;
    [SerializeField] private float cantidadPorPlantaInsecticida1;
    [SerializeField] private float cantidadPorPlantaInsecticida2;
    [SerializeField] private float cantidadPorPlantaFungicida;
    [SerializeField] private TMP_InputField[] fieldsEnfermedades;

    [Header("Cantidades de productos de control de malezas")]
    [System.NonSerialized]private float cantidadHerbicida1;
    [System.NonSerialized]private float cantidadHerbicida2;
    [SerializeField] private float cantidadPorPlantaHerbicida1;
    [SerializeField] private float cantidadPorPlantaHerbicida2;
    [SerializeField] private TMP_InputField[] fieldsMalezas;

    [Header("Cantidad de agua del cultivo")]    
    [SerializeField] private float cantidadAguaCultivo = 0;
    [SerializeField] private TMP_InputField fieldVolumenAgua;

    [Header("Cantidad de productos podas")]
    [System.NonSerialized] private float cantidadTijerasG = 2;
    [System.NonSerialized] private float cantidadTijerasP = 2;
    [SerializeField] private TMP_InputField[] fieldsPodas;

    [Header("Cantidad de productos tutorado")]
    [System.NonSerialized] private float cantidadEstacas;
    [System.NonSerialized] private float cantidadRollosCabuya;
    [SerializeField] private TMP_InputField[] fieldsTutorado;

    [Header("Cantidad de plantas")]
    [SerializeField] private TMP_InputField fieldCantidadPlantas;

    [Header("Cantidad de jornales actividades invernadero")]
    [SerializeField] private TMP_Dropdown[] dropsHumanResourseGreenHouse;
    private int cantidadJornalesFertilizacion;
    private int cantidadJornalesPlagasEnfermedades;
    private int cantidadJornalesControlMalezas;
    private int cantidadJornalesTutotizado;
    private int cantidadJornalesPodasFormacion;
    private int cantidadJornalesPodasSanitarias;
    private List<int> valoresCantidadJornalesInvernadero = new List<int>();

    [Header("CAntidad de jornales actividades germinadero")]
    private float cantidadJornalesGerminadero;
    [SerializeField] private TMP_Dropdown dropGerminator;

    private float valueToDivideCalification = 1f / 9;

    [Header("Porcentaje error")]
    [SerializeField] private float valueError;

    private bool duplicateValue = false;
    private bool duplicateValue1 = false;
    private bool duplicateValue2 = false;
    private bool duplicateValue3 = false;
    private bool duplicateValue4 = false;
    private bool duplicateValue5 = false;
    private bool duplicateValue6 = false;
    private bool duplicateValue7 = false;
    private bool duplicateValue8 = false;

    public void CalculoInsumosFertilizacion()
    {
        if(CheckFields(fieldsFertilizacion))
        {
            cantidadAbono1 = CountProductForUse(myValuesProductosManagerP2.frecuenciaFertilization, cantidadPorPlantaAbono1);
            cantidadAbono2 = CountProductForUse(myValuesProductosManagerP2.frecuenciaFertilization, cantidadPorPlantaAbono2);
            cantidadAbono3 = CountProductForUse(myValuesProductosManagerP2.frecuenciaFertilization, cantidadPorPlantaAbono3);
            cantidadMateriaOrganica = CountProductForUse(myValuesProductosManagerP2.frecuenciaFertilization, cantidadPorPlantaMateria);
            
            float auxFertilizacion = float.Parse(fieldsFertilizacion[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion1 = float.Parse(fieldsFertilizacion[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion2 = float.Parse(fieldsFertilizacion[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion3 = float.Parse(fieldsFertilizacion[3].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrect = auxFertilizacion >= (cantidadAbono1 / valueError) && auxFertilizacion <= (cantidadAbono1 * valueError) ? true : false;
            bool valueCorrect1 = auxFertilizacion1 >= (cantidadAbono2 / valueError) && auxFertilizacion1 <= (cantidadAbono2 * valueError) ? true : false;
            bool valueCorrect2 = auxFertilizacion2 >= (cantidadAbono3 / valueError) && auxFertilizacion2 <= (cantidadAbono3 * valueError) ? true : false;
            bool valueCorrect3 = auxFertilizacion3 >= (cantidadMateriaOrganica / valueError) && auxFertilizacion3 <= (cantidadMateriaOrganica * valueError) ? true : false;

            if(valueCorrect && valueCorrect1 && valueCorrect2 && valueCorrect3)
            {
                if(!duplicateValue)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue = true;
                }
            }
            else
            {
                duplicateValue = false;
            }
        }
    }

    public void CalculoInsumosPlagasEnfermedades()
    {
        if(CheckFields(fieldsEnfermedades))
        {
            cantidadInsecticida1 = CountProductForUse(myValuesProductosManagerP2.frecuenciaPlagasEnfermedades, cantidadPorPlantaInsecticida1);
            cantidadInsecticida2 = CountProductForUse(myValuesProductosManagerP2.frecuenciaPlagasEnfermedades, cantidadPorPlantaInsecticida2);
            cantidadFungicida = CountProductForUse(myValuesProductosManagerP2.frecuenciaPlagasEnfermedades, cantidadPorPlantaFungicida);

            float auxEnfermedades = float.Parse(fieldsEnfermedades[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxEnfermedades1 = float.Parse(fieldsEnfermedades[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxEnfermedades2 = float.Parse(fieldsEnfermedades[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectE = auxEnfermedades >= (cantidadInsecticida1 / valueError) && auxEnfermedades <= (cantidadInsecticida1 * valueError) ? true : false;
            bool valueCorrect1E = auxEnfermedades1 >= (cantidadInsecticida2 / valueError) && auxEnfermedades1 <= (cantidadInsecticida2 * valueError) ? true : false;
            bool valueCorrect2E = auxEnfermedades2 >= (cantidadFungicida / valueError) && auxEnfermedades2 <= (cantidadFungicida * valueError) ? true : false;

            if(valueCorrectE && valueCorrect1E && valueCorrect2E)
            {
                if(!duplicateValue1)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue1 = true;
                }
            }
            else
            {
                duplicateValue1 = false;
            }
        }
    }

    public void CalculoInsumosMalezas()
    {
        if(CheckFields(fieldsMalezas))
        {
            cantidadHerbicida1 = CountProductForUse(myValuesProductosManagerP2.frecuenciaMalezas, cantidadPorPlantaHerbicida1);
            cantidadHerbicida2 = CountProductForUse(myValuesProductosManagerP2.frecuenciaMalezas, cantidadPorPlantaHerbicida2);

            float auxMalezas = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxMalezas1 = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectM = auxMalezas >= (cantidadHerbicida1 / valueError) && auxMalezas <= (cantidadHerbicida1 * valueError) ? true : false;
            bool valueCorrect1M = auxMalezas1 >= (cantidadHerbicida2 / valueError) && auxMalezas1 <= (cantidadHerbicida2 * valueError) ? true : false;

            if(valueCorrectM && valueCorrect1M)
            {
                if(!duplicateValue2)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue2 = true;
                }
            }
            else
            {
                duplicateValue2 = false;
            }
        }
    }

    public void CalculoCantidadAgua()
    {
        if(fieldVolumenAgua.text != "")
        {
            float countAgua = float.Parse(fieldVolumenAgua.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            cantidadAguaCultivo = (myValuesProductosManagerP2.areaCultivo * (55.52f + 94.04f + 147.64f + 102.28f)) / 1000;

            if(countAgua >= (cantidadAguaCultivo / valueError) && countAgua <= (cantidadAguaCultivo * valueError))
            {
                if(!duplicateValue3)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue3 = true;
                }
            }
            else
            {
                duplicateValue3 = false;
            }
        }
    }

    public void CalculoInsumosPodas()
    {
        if(CheckFields(fieldsPodas))
        {
            if(int.Parse(fieldsPodas[0].text) == cantidadTijerasG && int.Parse(fieldsPodas[1].text) == cantidadTijerasP)
            {
                if(!duplicateValue4)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue4 = true;
                }
            }
            else
            {
                duplicateValue4 = false;
            }
        }
    }

    public void CalculoInsumosTutorado()
    {
        if(CheckFields(fieldsTutorado))
        {
            cantidadEstacas = myValuesProductosManagerP2.cantidadPlantasDensidad;
            cantidadRollosCabuya = myValuesProductosManagerP2.areaCultivo;

            float auxTutorado = float.Parse(fieldsTutorado[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxTutorado1 = float.Parse(fieldsTutorado[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectT = auxTutorado >= (cantidadEstacas / valueError) && auxTutorado <= (cantidadEstacas * valueError) ? true : false;
            bool valueCorrect1T = auxTutorado1 >= (cantidadRollosCabuya / valueError) && auxTutorado1 <= (cantidadRollosCabuya * valueError) ? true : false;

            if(valueCorrectT && valueCorrect1T)
            {
                if(!duplicateValue5)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue5 = true;
                }
            }
            else
            {
                duplicateValue5 = false;
            }
        }
    }

    public void CalculoCantidadPlantas()
    {
        if(fieldCantidadPlantas.text != "")
        {
            float countPlantas = float.Parse(fieldCantidadPlantas.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float aux = myValuesProductosManagerP2.cantidadPlantasDensidad;
            if(countPlantas >= (aux / valueError) && countPlantas <= (aux * valueError))
            {
                if(!duplicateValue6)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue6 = true;
                }
            }
            else
            {
                duplicateValue6 = false;
            }
        }
    }

    public void CalculoJornalesActividadesInvernadero()
    {
        int countFieldCorrect = 0;
        cantidadJornalesFertilizacion = (120 / myValuesProductosManagerP2.frecuenciaFertilization);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesFertilizacion);
        cantidadJornalesPlagasEnfermedades = (120 / myValuesProductosManagerP2.frecuenciaPlagasEnfermedades);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesPlagasEnfermedades);
        cantidadJornalesControlMalezas = (120 / myValuesProductosManagerP2.frecuenciaMalezas);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesControlMalezas);
        cantidadJornalesTutotizado = (120 / myValuesProductosManagerP2.frecuenciaTutorizado);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesTutotizado);
        cantidadJornalesPodasFormacion = (120 / myValuesProductosManagerP2.frecuenciaPodasFormacion);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesPodasFormacion);
        cantidadJornalesPodasSanitarias = (120 / myValuesProductosManagerP2.frecuenciaPodasSanitarias);
        valoresCantidadJornalesInvernadero.Add(cantidadJornalesPodasSanitarias);

        for(int i = 0; i < valoresCantidadJornalesInvernadero.Count; i++)
        {
            int valueUser = int.Parse(dropsHumanResourseGreenHouse[i].captionText.text);
            if(valueUser >= (valoresCantidadJornalesInvernadero[i] / valueError) && valueUser <= (valoresCantidadJornalesInvernadero[i] * valueError))
            {
                countFieldCorrect++;
            }
        }

        if(countFieldCorrect == dropsHumanResourseGreenHouse.Length)
        {
            if(!duplicateValue7)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue7 = true;
                }
            }
            else
            {
                duplicateValue7 = false;
            }
    }

    public void CalcularJornalesGerminator()
    {
        cantidadJornalesGerminadero = (120 / 5);

        int auxJornalGerminadero = int.Parse(dropGerminator.captionText.text);

        if(auxJornalGerminadero >= (cantidadJornalesGerminadero / valueError) && auxJornalGerminadero <= (cantidadJornalesGerminadero * valueError))
        {
            if(!duplicateValue8)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue8 = true;
                }
            }
            else
            {
                duplicateValue8 = false;
            }
    }

    private float CountProductForUse(float frecuenciaApli ,float cantiForPlanta)
    {
        float cantidadProduc = (myValuesProductosManagerP2.cantidadPlantasDensidad * cantiForPlanta) / 1000;
        return cantidadProduc;
    }

    bool CheckFields(TMP_InputField[] fieldTest)
    {
        int test = 0;

        for(int i = 0; i < fieldTest.Length; i++)
        {
            if(fieldTest[i].text == "")
            {
                test = 1;
            }
        }
        return test > 0 ? false : true;
    }
}
