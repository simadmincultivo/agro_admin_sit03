﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSActiveZone;
using NSInterfaz;

public class StrctureControllerP2 : AbstractObject2D
{
    public bool greenHouseInside;
    public bool germinatorInside;

    [Header("Referencia a scripts")]
    [SerializeField] private ControllArrowBack myControllArrowBack;

    [Header("Objetos practica 2")]
    [SerializeField] private GameObject containerInsideCellar;
    [SerializeField] private GameObject BackgroundHangar;
    [SerializeField] private GameObject BackgroundGreenHouse;
    [SerializeField] private GameObject BackgroundGerminator;
    [SerializeField] private GameObject ButtonFertilizer;
    [SerializeField] private GameObject ButtonWater;

    private bool hangarInside;
    

    void Start()
    {
        actualStageIndex = 1;
        ActivateAllActiveZonesOfActualStage(true);
    }

    

    public override void OnMoving()
    {
    }

    public override void OnTab()
    {
    }

    public override void OnTabOverActiveZone(string argNameActiveZone)
    {
        if(argNameActiveZone.Equals("ZoneCellar"))
        {
            DisableZones(1);
            myControllArrowBack.selectedCellarP2 = true;
            containerInsideCellar.SetActive(true);
            ButtonFertilizer.SetActive(true);
            ButtonWater.SetActive(false);
            UpdateStateAndActiveZones(2);
        }
        else if(argNameActiveZone.Equals("ZoneFertilizer"))
        {
            PanelFertilizacion.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneSuppliesWeeds"))
        {
            PanelPlagasEnfermedades.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneSuppliesDiseases"))
        {
            PanelControlMalezas.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneToolsPruning"))
        {
            PanelPodas.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneTutored"))
        {
            PanelTutorado.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneHangarP2"))
        {
            DisableZones(1);
            BackgroundHangar.SetActive(true);
            PanelIngreseVolumenAgua.Instance.ShowPanel();
            hangarInside = true;
            myControllArrowBack.selectedHangarP2 = true;
            ButtonWater.SetActive(true);
            ButtonFertilizer.SetActive(false);
        }
        else if(argNameActiveZone.Equals("ZoneGreenHouseP2"))
        {
            DisableZones(1);
            BackgroundGreenHouse.SetActive(true);
            myControllArrowBack.selectedGreenHouseP2 = true;
            germinatorInside = false;
            greenHouseInside = true;
        }
        else if(argNameActiveZone.Equals("ZoneGerminatorP2"))
        {
            DisableZones(1);
            BackgroundGerminator.SetActive(true);
            PanelIngreseCantidadPlantulas.Instance.ShowPanel();
            myControllArrowBack.selectedGerminatorP2 = true;
            greenHouseInside = false;
            germinatorInside = true;
        }
    }

    protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
    {
    }

    protected override void OnReleaseObjectOverNothing()
    {
    }

    void UpdateStateAndActiveZones(int state)
    {
        actualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(true);
    }

    public void EnablePanelResourceHuman()
    {
        if(greenHouseInside)
        {
            PanelRecursoHumano1Situacion2.Instance.ShowPanel();
            //greenHouseInside = false;
        }
        else if(germinatorInside)
        {
            PanelRecursoHumano2Situacion2.Instance.ShowPanel();
            //germinatorInside = false;
        }
    }

    void DisableZones(int state)
    {
        actualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(false);
    }
}
