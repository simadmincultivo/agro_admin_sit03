﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSInterfaz;
using NSInterfazAvanzada;
using NSBoxMessage;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class RegisterDatesP4 : MonoBehaviour
{
    [Header("Variable calificación registro de datos")]
    public float calificacion2;
    
    [Header("Referencia a inputField de registro de datos")]
    public TMP_InputField[] fieldRegisterDatesP4;

    [Header("Referencia a scripts")]
    [SerializeField] private InstantiateController myInstanceController;
    [SerializeField] private CalculoCantidadProductosP4 myCalculoCantidadProductos;

    [Header("Referencia a objetos en escena")]
    [SerializeField] private GameObject[] xInFields;

    [Header("Porcentaje de erro")]
    [SerializeField] private float valueError;

    [Header("Variables para intentos")]
    [SerializeField] private TextMeshProUGUI AttemptsText;
    [SerializeField] private SOSessionData soSessionData;
    private int countAttempts = 1;

    private float sumCostFijos;
    private float sumCostVariables;
    private List<float> listValuesToCompare = new List<float>();
    private bool canContinius;

    public void ButtonCheckDates()
    {
        SumPriceOfProductsInBuilds();
        CheckValuesInRegisterAndTable();
    }

    void SumPriceOfProductsInBuilds()
    {
        listValuesToCompare.Clear();
        sumCostFijos = 0;
        sumCostVariables = 0;

        for(int i = 0; i < myInstanceController.listCostosFijosP4.Count; i++)
        {
            sumCostFijos += myInstanceController.listCostosFijosP4[i];
        }
        listValuesToCompare.Add(sumCostFijos);

        for(int i = 0; i < myInstanceController.listCostosVariablesP4.Count; i++)
        {
            sumCostVariables += myInstanceController.listCostosVariablesP4[i];
        }
        listValuesToCompare.Add(sumCostVariables);
        listValuesToCompare.Add(myInstanceController.countToView);
    }

    void CheckValuesInRegisterAndTable()
    {
        int countErrors = 0;
        calificacion2 = 0;
        for(int i = 0; i < fieldRegisterDatesP4.Length; i++)
        {
            if(fieldRegisterDatesP4[i].text != "")
            {
                float valueUser = float.Parse(fieldRegisterDatesP4[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                float valueCompare = listValuesToCompare[i];
                if(valueUser >= (valueCompare / valueError) && valueUser <= (valueCompare * valueError))
                {
                    xInFields[i].SetActive(false);
                    calificacion2 += 1f / fieldRegisterDatesP4.Length;
                }
                else
                {
                    xInFields[i].SetActive(true);
                    countErrors++;
                }
            }
            else 
            {
                xInFields[i].SetActive(true);
                countErrors++;
            }
        }

        if(countErrors > 0)
        {
            PanelMensajeIncorrecto.Instance.ShowPanel();
            countAttemptsMetod();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosCorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, null);
            canContinius = true;
        }
    }

    public void ButtonReport()
    {
        if(canContinius)
        {
            PanelRegistroDatosSituacion4.Instance.ShowPanel(false);
            PanelInterfazEvaluacion.Instance.ShowPanel();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosIncorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ButtonAceptar, null);
        }
    }

    void ButtonAceptar()
    {
        PanelRegistroDatosSituacion4.Instance.ShowPanel(false);
        PanelInterfazEvaluacion.Instance.ShowPanel();
        countAttemptsMetod();
    }

    public void countAttemptsMetod()
    {
        countAttempts += 1;
        AttemptsText.text = countAttempts.ToString();
        soSessionData.quantityAttempts = (byte) countAttempts;
    }
}
