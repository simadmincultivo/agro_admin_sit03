﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ViewDatesParametros : MonoBehaviour
{
    [Header("Referencias a script")]
    [SerializeField] private ValuesOfProductsManagerP4 myValuesProductosP4;

    [Header("Fields para mostrar parametros")]
    [SerializeField] private TextMeshProUGUI[] textViewParameter;

    [Header("Referencia a boton")]
    [SerializeField] private GameObject buttonViewParameters;

    public void ExcuteEventSetParemeters()
    {
        StartCoroutine(SetDatesParameter());
    }

    IEnumerator SetDatesParameter()
    {
        textViewParameter[0].text = myValuesProductosP4.terrainLargo.ToString();
        textViewParameter[1].text = myValuesProductosP4.terrainAncho.ToString();
        textViewParameter[2].text = myValuesProductosP4.tiempoCultivo.ToString();
        textViewParameter[3].text = myValuesProductosP4.distanciaCalleMayor.ToString();
        textViewParameter[4].text = myValuesProductosP4.distanciaPlantas.ToString();
        textViewParameter[5].text = myValuesProductosP4.repFertilizacion.ToString();
        textViewParameter[6].text = myValuesProductosP4.repRiego.ToString();
        textViewParameter[7].text = myValuesProductosP4.repTutorado.ToString();
        textViewParameter[8].text = myValuesProductosP4.repPodasFormacion.ToString();
        textViewParameter[9].text = myValuesProductosP4.repPodasSanitarias.ToString();
        textViewParameter[10].text = myValuesProductosP4.repPlagasEnfermedades.ToString();
        textViewParameter[11].text = myValuesProductosP4.repMalezas.ToString();
        //gameObject.SetActive(true);

        yield return new WaitForSeconds(5f);

        buttonViewParameters.SetActive(true);
        gameObject.SetActive(false);
    }
}
