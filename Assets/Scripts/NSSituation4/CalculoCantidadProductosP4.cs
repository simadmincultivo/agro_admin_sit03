﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSBoxMessage;

public class CalculoCantidadProductosP4 : MonoBehaviour
{
    [Header("Variable calificación")]
    public float calificacion1;

    [Header("Referencia a Scripts")]
    [SerializeField] private ValuesOfProductsManagerP4 myValuesProductosManagerP4;

    [Header("Cantidades de productos y fields de fertilización")]
    private float cantidadAbono1;
    private float cantidadAbono2;
    private float cantidadAbono3;
    private float cantidadMateriaOrganica;
    [SerializeField] private float cantidadPorPlantaAbono1;
    [SerializeField] private float cantidadPorPlantaAbono2;
    [SerializeField] private float cantidadPorPlantaAbono3;
    [SerializeField] private float cantidadPorPlantaMateria;
    [SerializeField] private TMP_InputField[] fieldsFertilizacion;

    [Header("Cantidades de productos de plagas y enfermedades")]
    private float cantidadInsecticida1;
    private float cantidadInsecticida2;
    private float cantidadFungicida;
    [SerializeField] private float cantidadPorPlantaInsecticida1;
    [SerializeField] private float cantidadPorPlantaInsecticida2;
    [SerializeField] private float cantidadPorPlantaFungicida;
    [SerializeField] private TMP_InputField[] fieldsEnfermedades;

    [Header("Cantidades de productos de control de malezas")]
    private float cantidadHerbicida1;
    private float cantidadHerbicida2;
    [SerializeField] private float cantidadPorPlantaHerbicida1;
    [SerializeField] private float cantidadPorPlantaHerbicida2;
    [SerializeField] private TMP_InputField[] fieldsMalezas;

    [Header("Cantidad de agua del cultivo")]    
    private float cantidadAguaCultivo;
    [SerializeField] private TMP_InputField fieldVolumenAgua;

    [Header("Cantidad de productos podas")]
    private float cantidadTijerasG = 2;
    private float cantidadTijerasP = 2;
    [SerializeField] private TMP_InputField[] fieldsPodas;

    [Header("Cantidad de productos tutorado")]
    private float cantidadEstacas;
    private float cantidadRollosCabuya;
    [SerializeField] private TMP_InputField[] fieldsTutorado;

    [Header("Cantidad de plantas")]
    [SerializeField] private TMP_InputField fieldCantidadPlantas;

    [Header("Cantidad de jornales actividades invernadero")]
    [SerializeField] private TMP_InputField[] fieldsHumanResourseGreenHouse;

    private float cantidadJornalesFertilizacion;
    private float cantidadJornalesPlagasEnfermedades;
    private float cantidadJornalesControlMalezas;
    private float cantidadJornalesTutotizado;
    private float cantidadJornalesPodasFormacion;
    private float cantidadJornalesPodasSanitarias;
    private List<float> valoresCantidadJornalesInvernadero = new List<float>();

    [Header("CAntidad de jornales actividades germinadero")]
    private float cantidadJornalesGerminadero;
    [SerializeField] private TMP_InputField fieldGerminator;

    private float valueToDivideCalification = 1f / 9;

    [Header("Porcentaje error")]
    [SerializeField] private float valueError;

    private bool duplicateValue = false;
    private bool duplicateValue1 = false;
    private bool duplicateValue2 = false;
    private bool duplicateValue3 = false;
    private bool duplicateValue4 = false;
    private bool duplicateValue5 = false;
    private bool duplicateValue6 = false;
    private bool duplicateValue7 = false;
    private bool duplicateValue8 = false;

    public void CalculoInsumosFertilizacion()
    {
        if(CheckFields(fieldsFertilizacion))
        {
            cantidadAbono1 = CountProductForUse(myValuesProductosManagerP4.repFertilizacion, cantidadPorPlantaAbono1);
            cantidadAbono2 = CountProductForUse(myValuesProductosManagerP4.repFertilizacion, cantidadPorPlantaAbono2);
            cantidadAbono3 = CountProductForUse(myValuesProductosManagerP4.repFertilizacion, cantidadPorPlantaAbono3);
            cantidadMateriaOrganica = CountProductForUse(myValuesProductosManagerP4.repFertilizacion, cantidadPorPlantaMateria);

            Debug.Log("Datos " + "/" + cantidadAbono1 + "/" + cantidadAbono2 + "/" + cantidadAbono3 + "/" +cantidadMateriaOrganica);

            float auxFertilizacion = float.Parse(fieldsFertilizacion[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion1 = float.Parse(fieldsFertilizacion[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion2 = float.Parse(fieldsFertilizacion[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxFertilizacion3 = float.Parse(fieldsFertilizacion[3].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrect = auxFertilizacion >= (cantidadAbono1 / valueError) && auxFertilizacion <= (cantidadAbono1 * valueError) ? true : false;
            bool valueCorrect1 = auxFertilizacion1 >= (cantidadAbono2 / valueError) && auxFertilizacion1 <= (cantidadAbono2 * valueError) ? true : false;
            bool valueCorrect2 = auxFertilizacion2 >= (cantidadAbono3 / valueError) && auxFertilizacion2 <= (cantidadAbono3 * valueError) ? true : false;
            bool valueCorrect3 = auxFertilizacion3 >= (cantidadMateriaOrganica / valueError) && auxFertilizacion3 <= (cantidadMateriaOrganica * valueError) ? true : false;

            if(valueCorrect && valueCorrect1 && valueCorrect2 && valueCorrect3)
            {
                if(!duplicateValue)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue = true;
                }
            }
            else
            {
                duplicateValue = false;
            }
        }
    }

    public void CalculoInsumosPlagasEnfermedades()
    {
        if(CheckFields(fieldsEnfermedades))
        {
            cantidadInsecticida1 = CountProductForUse(myValuesProductosManagerP4.repPlagasEnfermedades, cantidadPorPlantaInsecticida1);
            cantidadInsecticida2 = CountProductForUse(myValuesProductosManagerP4.repPlagasEnfermedades, cantidadPorPlantaInsecticida2);
            cantidadFungicida = CountProductForUse(myValuesProductosManagerP4.repPlagasEnfermedades, cantidadPorPlantaFungicida);

            float auxEnfermedades = float.Parse(fieldsEnfermedades[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxEnfermedades1 = float.Parse(fieldsEnfermedades[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxEnfermedades2 = float.Parse(fieldsEnfermedades[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectE = auxEnfermedades >= (cantidadInsecticida1 / valueError) && auxEnfermedades <= (cantidadInsecticida1 * valueError) ? true : false;
            bool valueCorrect1E = auxEnfermedades1 >= (cantidadInsecticida2 / valueError) && auxEnfermedades1 <= (cantidadInsecticida2 * valueError) ? true : false;
            bool valueCorrect2E = auxEnfermedades2 >= (cantidadFungicida / valueError) && auxEnfermedades2 <= (cantidadFungicida * valueError) ? true : false;

            if(valueCorrectE && valueCorrect1E && valueCorrect2E)
            {
                if(!duplicateValue1)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue1 = true;
                }
            }
            else
            {
                duplicateValue1 = false;
            }
        }
    }

    public void CalculoInsumosMalezas()
    {
        if(CheckFields(fieldsMalezas))
        {
            cantidadHerbicida1 = CountProductForUse(myValuesProductosManagerP4.repMalezas, cantidadPorPlantaHerbicida1);
            cantidadHerbicida2 = CountProductForUse(myValuesProductosManagerP4.repMalezas, cantidadPorPlantaHerbicida2);

            float auxMalezas = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxMalezas1 = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectM = auxMalezas >= (cantidadHerbicida1 / valueError) && auxMalezas <= (cantidadHerbicida1 * valueError) ? true : false;
            bool valueCorrect1M = auxMalezas1 >= (cantidadHerbicida2 / valueError) && auxMalezas1 <= (cantidadHerbicida2 * valueError) ? true : false;

            if(valueCorrectM && valueCorrect1M)
            {
                if(!duplicateValue2)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue2 = true;
                }
            }
            else
            {
                duplicateValue2 = false;
            }
        }
    }

    public void CalculoCantidadAgua()
    {
        if(fieldVolumenAgua.text != "")
        {
            float countAgua = float.Parse(fieldVolumenAgua.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            cantidadAguaCultivo = (myValuesProductosManagerP4.areaCultivo * (55.52f + 94.04f + 147.64f + 102.28f)) / 1000;
            if(countAgua >= (cantidadAguaCultivo / valueError) && countAgua <= (cantidadAguaCultivo * valueError))
            {
                if(!duplicateValue3)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue3 = true;
                }
            }
            else
            {
                duplicateValue3 = false;
            }
        }
    }

    public void CalculoInsumosPodas()
    {
        if(fieldsPodas[0].text != "")
        {
            if(int.Parse(fieldsPodas[0].text) == cantidadTijerasG && int.Parse(fieldsPodas[1].text) == cantidadTijerasP)
            {
                if(!duplicateValue4)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue4 = true;
                }
            }
            else
            {
                duplicateValue4 = false;
            }
        }
    }

    public void CalculoInsumosTutorado()
    {
        if(CheckFields(fieldsTutorado))
        {
            cantidadEstacas = myValuesProductosManagerP4.cantidadPlantasDensidad;
            cantidadRollosCabuya = myValuesProductosManagerP4.areaCultivo;

            float auxTutorado = float.Parse(fieldsTutorado[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float auxTutorado1 = float.Parse(fieldsTutorado[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            bool valueCorrectT = auxTutorado >= (cantidadEstacas / valueError) && auxTutorado <= (cantidadEstacas * valueError) ? true : false;
            bool valueCorrect1T = auxTutorado1 >= (cantidadRollosCabuya / valueError) && auxTutorado1 <= (cantidadRollosCabuya * valueError) ? true : false;

            if(valueCorrectT && valueCorrect1T)
            {
                if(!duplicateValue5)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue5 = true;
                }
            }
            else
            {
                duplicateValue5 = false;
            }
        }
    }

    public void CalculoCantidadPlantas()
    {
        if(fieldCantidadPlantas.text != "")
        {
            float countPlantas = float.Parse(fieldCantidadPlantas.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            float aux = myValuesProductosManagerP4.cantidadPlantasDensidad;
            if(countPlantas >= (aux / valueError) && countPlantas <= (aux * valueError))
            {
                if(!duplicateValue6)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue6 = true;
                }
            }
            else
            {
                duplicateValue6 = false;
            }
        }
    }

    public void CalculoJornalesActividadesInvernadero()
    {
        if(CheckFields(fieldsHumanResourseGreenHouse))
        {
            int countFieldCorrect = 0;
            valoresCantidadJornalesInvernadero.Clear();
            cantidadJornalesFertilizacion = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repFertilizacion);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesFertilizacion);
            cantidadJornalesPlagasEnfermedades = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repPlagasEnfermedades);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesPlagasEnfermedades);
            cantidadJornalesControlMalezas = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repMalezas);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesControlMalezas);
            cantidadJornalesTutotizado = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repTutorado);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesTutotizado);
            cantidadJornalesPodasFormacion = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repPodasFormacion);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesPodasFormacion);
            cantidadJornalesPodasSanitarias = (myValuesProductosManagerP4.tiempoCultivo / myValuesProductosManagerP4.repPodasSanitarias);
            valoresCantidadJornalesInvernadero.Add(cantidadJornalesPodasSanitarias);

            for(int i = 0; i < valoresCantidadJornalesInvernadero.Count; i++)
            {
                float valueUser = float.Parse(fieldsHumanResourseGreenHouse[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                if(valueUser >= (valoresCantidadJornalesInvernadero[i] / valueError) && valueUser <= (valoresCantidadJornalesInvernadero[i] * valueError))
                {
                    countFieldCorrect++;
                }
            }

            if(countFieldCorrect == fieldsHumanResourseGreenHouse.Length)
            {
                if(!duplicateValue7)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue7 = true;
                }
            }
            else
            {
                duplicateValue7 = false;
            }
        }
    }

    public void CalcularJornalesGerminator()
    {
        if(fieldGerminator.text != "")
        {
            cantidadJornalesGerminadero = (myValuesProductosManagerP4.tiempoCultivo / 5);
            float valueUser = float.Parse(fieldGerminator.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            if(valueUser >= (cantidadJornalesGerminadero / valueError) && valueUser <= (cantidadJornalesGerminadero * valueError))
            {
                if(!duplicateValue8)
                {
                    calificacion1 += valueToDivideCalification;
                    duplicateValue8 = true;
                }
            }
            else
            {
                duplicateValue8 = false;
            }
        }
    }

    private float CountProductForUse(float frecuenciaApli ,float cantiForPlanta)
    {
        float cantidadProduc = (myValuesProductosManagerP4.cantidadPlantasDensidad * cantiForPlanta) / 1000;
        return cantidadProduc;
    }

    bool CheckFields(TMP_InputField[] fieldTest)
    {
        int test = 0;

        for(int i = 0; i < fieldTest.Length; i++)
        {
            if(fieldTest[i].text == "")
            {
                test = 1;
            }
        }
        return test > 0 ? false : true;
    }
}
