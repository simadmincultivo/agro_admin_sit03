﻿#pragma warning disable 0649
#pragma warning disable 0660
#pragma warning disable 0661
#pragma warning disable 0618

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using SimpleJSON;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.Events;
using NSScriptableEvent;
using NSScriptableValues;
#if !UNITY_WEBGL
using ValidacionMenu;

#endif

namespace NSSeguridad
{
    public class Seguridad : MonoBehaviour
    {
        #region members

        [SerializeField] public SOSecurityData soSecurityData;

        [SerializeField] public SOSecurityConfiguration soSecurityConfiguration;

        [SerializeField] private ScriptableEventInt seOnLoginInt;

        [SerializeField] private ScriptableEvent seOnInputsLogin4InputsCorrect;

        [SerializeField] private ScriptableEvent seOnInputsLogin2InputsCorrect;

        [SerializeField] private ScriptableEvent seOnPanelWellcomeClose;
        
        private float timeOut = 2;

        #endregion

        #region  monoBehaviour

        private void Awake()
        {
            CargarExternalXML();
        }

        private void OnEnable()
        {
            seOnPanelWellcomeClose.Subscribe(IniciarSimulador);
            seOnInputsLogin2InputsCorrect.Subscribe(LoginAula);
            seOnInputsLogin4InputsCorrect.Subscribe(LoginOffLine);
        }

        private void OnDisable()
        {
            seOnPanelWellcomeClose.Unsubscribe(IniciarSimulador);
            seOnInputsLogin2InputsCorrect.Unsubscribe(LoginAula);
            seOnInputsLogin4InputsCorrect.Unsubscribe(LoginOffLine);
        }

        #endregion

        #region public methods

        /// <summary>
        /// Determina si el simulador comienza en modo online o offline
        /// </summary>
        public void IniciarSimulador()
        {
#if UNITY_WEBGL
            CargarExternalXML();

            if(soSecurityConfiguration.isLtiActive)
                LtiNotificacion();
#endif

            if (soSecurityConfiguration.isSecurityOn)//Si seguridad esta activada
            {
#if !UNITY_WEBGL

                if (Validacion.Validar())//Funcion que valida la licencia
                {
                    if (soSecurityConfiguration.isModeClassroom)//si modo aula esta activado
                        PanelLogin2Campos.Instance.ShowPanel();//muestro el login 2 campos
                    else
                        PanelInterfazLogin4Campos.Instance.ShowPanel();//si no esta activado el modo aula, muestro el login 4 campos
                }
                else
                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextBoxAlertSeguridad"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);//mensaje error de licencia 
#endif
            }
            else
            {
                if (soSecurityConfiguration.isModeClassroom)
                    PanelLogin2Campos.Instance.ShowPanel();
                else
                    PanelInterfazLogin4Campos.Instance.ShowPanel();
            }
        }
        
        /// <summary>
        /// Se ejecuta cuando se conecta con el formulario de 2 campos
        /// </summary>
        public void LoginAula()
        {
            AulaLoginRequest();
        }

        /// <summary>
        /// Se ejecuta cuando se conecta con el formulario de 4 campos
        /// </summary>
        public void LoginOffLine()
        {
           // soSecurityData.aulaClassName = DiccionarioIdiomas.Instance.Traducir("NombreCurso");
            soSecurityData.isLogin = true;
            seOnLoginInt.ExecuteEvent(0);
        }

        #endregion

        #region courutines

        /// <summary>
        /// corrutina que verifica las diferentes opciones de la licencia y notifica de cual quier problema  
        /// </summary>
        private IEnumerator WaitForRequest(WWW www)
        {
            Debug.Log("in waitforrequest");
            float timer = 0;
            bool failed = false;

            while (!www.isDone)
            {
                if (timer > timeOut)
                {
                    failed = true;
                    break;
                }

                timer += Time.deltaTime;
                yield return null;
            }

            if (failed)
            {
                www.Dispose();

                if (PlayerPrefs.HasKey("attempts"))
                {
                    int attempts = PlayerPrefs.GetInt("attempts");

                    if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                    {
                        attempts = attempts + 1;
                        PlayerPrefs.SetInt("attempts", attempts);
                        PlayerPrefs.Save();

                        if (soSecurityConfiguration.isModeClassroom)
                            ActivePanelLogin();
                    }
                    else
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeMuchoTiempo"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                }
                else
                    BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("MensajeOPPs"), DiccionarioIdiomas.Instance.Traducir("TextValidar"), DiccionarioIdiomas.Instance.Traducir("TextBotonCancelar"), GetLicenseData, Cerrar);
            }
            else
            {
                if (string.IsNullOrEmpty(www.error))
                {
                    Debug.Log("WWW Ok!: " + www.text);
                    var _data = JSON.Parse(www.text);

                    switch (_data["result"].Value)
                    {
                        case "error":

                            switch (_data["message_id"].AsInt)
                            {
                                case 1:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeNumeroLIcencia"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 2:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLicenciaSinActivar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 3:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLicenciaSinActivar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 4:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeLicenciaCaducada"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 5:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeLicenciaNoPermite"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 6:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeNumeroActivaciones"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;

                                case 7:
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeResultadoInvalido"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                                    break;
                            }

                            if (PlayerPrefs.HasKey("attempts"))
                            {
                                PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                PlayerPrefs.Save();
                            }

                            break;

                        case "success":
                            PlayerPrefs.SetInt("attempts", 0);
                            PlayerPrefs.SetInt("offline_attempts", _data["offline_attempts"].AsInt);
                            PlayerPrefs.Save();
                            soSecurityData.isLogin = true;

                            if (soSecurityConfiguration.isModeClassroom)
                                ActivePanelLogin();

                            break;
                    }
                }
                else
                {
                    if (PlayerPrefs.HasKey("attempts"))
                    {
                        int attempts = PlayerPrefs.GetInt("attempts");

                        if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                        {
                            attempts = attempts + 1;
                            PlayerPrefs.SetInt("attempts", attempts);
                            PlayerPrefs.Save();

                            if (soSecurityConfiguration.isModeClassroom)
                                ActivePanelLogin();
                        }
                        else
                            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("MensajeMuchoTiempo"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), Cerrar);
                    }
                    else
                        BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("MensajeOPPs"), DiccionarioIdiomas.Instance.Traducir("TextValidar"), DiccionarioIdiomas.Instance.Traducir("TextBotonCancelar"), GetLicenseData, Cerrar);
                }
            }
        }

        /// <summary>
        /// corrutinas que mmaneja el logueo desde el servidor remoto
        /// </summary>
        private IEnumerator WaitForRequestAula(WWW www)
        {
            yield return www;

            if (www.error == null)
            {
                byte[] decodedBytes = Convert.FromBase64String(www.text);
                string decodedText = Encoding.UTF8.GetString(decodedBytes);
                Debug.Log(decodedText);
                var _data = JSON.Parse(decodedText);

                if (_data["state"] != null)
                {
                    if (_data["state"].Value == "true")
                    {
                        if (_data["res_code"] != null)
                        {
                            switch (_data["res_code"].Value)
                            {
                                case "INVALID_USER_PASS":
                                    seOnLoginInt.ExecuteEvent(0);
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajePassword"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LAB_NOT_ASSIGNED":
                                    seOnLoginInt.ExecuteEvent(0);
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeLAB_NOT_ASSIGNED"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LOGIN_OK":
                                    soSecurityData.aulaUserFirstName = _data["name"].Value;
                                    soSecurityData.aulaUserLastName = _data["last_name"].Value;
                                    soSecurityData.aulaClassId = _data["class_group"].Value;
                                    soSecurityData.aulaSchoolName = _data["school_name"].Value;
                                    soSecurityData.aulaUsername = soSecurityData.aulaUserFirstName + " " + soSecurityData.aulaUserLastName;
                                    soSecurityData.aulaClassName = DiccionarioIdiomas.Instance.Traducir("TextPlaceHolderCurso");
                                    soSecurityData.isLogin = true;
                                    soSecurityConfiguration.isConectionActive = true;
                                    seOnLoginInt.ExecuteEvent(1);
                                    break;

                                case "DB_EXCEPTION":
                                    seOnLoginInt.ExecuteEvent(0);
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeErrorDB"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                                    break;

                                case "LICENSE_EXPIRED":
                                    seOnLoginInt.ExecuteEvent(0);
                                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeProblemasLicenciaGestor"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), CambioModoOffline);
                                    break;
                            }
                        }
                        else
                        {
                            seOnLoginInt.ExecuteEvent(0);
                            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                        }
                    }
                    else
                    {
                        seOnLoginInt.ExecuteEvent(0);
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                    }
                }
                else
                {
                    seOnLoginInt.ExecuteEvent(0);
                    BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeDelServidorRespuestaInvalida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                }
            }
            else
            {
                seOnLoginInt.ExecuteEvent(2);
                BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeReIntentar"),  DiccionarioIdiomas.Instance.Traducir("TextOffline"), DiccionarioIdiomas.Instance.Traducir("TextReintentar"), AulaLoginRequest, CambioModoOffline);
            }
        }

        #endregion

        #region Private methods
        
        /// <summary>
        /// Activa el panel de login correspondiente de 2 o 4 campos
        /// </summary>
        private void ActivePanelLogin()
        {
            if (soSecurityConfiguration.isModeClassroom)
                PanelLogin2Campos.Instance.ShowPanel();
            else
                PanelInterfazLogin4Campos.Instance.ShowPanel();
        }

        /// <summary>
        /// inicia todo el proceso de obtencio de licencia
        /// </summary>
        private void GetLicenseData()
        {
            var bundle_id = "com.cloudlabs.seccionesconicas";
            var device_id = "";
            var cmdInfo = "";

            if (Application.platform == RuntimePlatform.Android)
            {
#if UNITY_ANDROID
                using (AndroidJavaClass jc = new AndroidJavaClass("com.cloudlabs.seccionesconicas"))
                {
                    cmdInfo = jc.CallStatic<string>("getLauncherURL");
                    device_id = jc.CallStatic<string>("getDeviceData");
                }
#endif
                if (cmdInfo != "no input data" && cmdInfo != "")
                {
                    soSecurityData.licenseNumber = cmdInfo.Substring(cmdInfo.IndexOf("LICENCIA=") + 9, 23);
                    bundle_id = cmdInfo.Substring(0, cmdInfo.IndexOf("://"));
                }
                else
                {
                    soSecurityData.licenseNumber = "";
                    //soSecurityData.licenseNumber = "53392-F5BD8-F445C-EB286";
                }
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                List<string> macsArray = new List<string>();
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                foreach (NetworkInterface adapter in nics)
                {
                    PhysicalAddress address = adapter.GetPhysicalAddress();
                    byte[] bytes = address.GetAddressBytes();
                    string mac = null;

                    for (int i = 0; i < bytes.Length; i++)
                    {
                        mac = string.Concat(mac + (string.Format("{0}", bytes[i].ToString("X2"))));
                        if (i != bytes.Length - 1)
                        {
                            mac = string.Concat(mac + "-");
                        }
                    }

                    if (mac != null)
                        macsArray.Add(mac);
                }

                device_id = string.Join(",", macsArray.ToArray());

                string[] args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                    soSecurityData.licenseNumber = args[1];
            }

            if (soSecurityData.licenseNumber != "")
            {
                if (PlayerPrefs.HasKey("attempts"))
                    soSecurityConfiguration.licenceUrl = "https://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(soSecurityData.licenseNumber) + "&primera_vez=false";
                else
                    soSecurityConfiguration.licenceUrl = "https://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(soSecurityData.licenseNumber) + "&primera_vez=true";

                Debug.Log(soSecurityConfiguration.licenceUrl);
                WWW www = new WWW(soSecurityConfiguration.licenceUrl);
                StartCoroutine(WaitForRequest(www));
            }
            else
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextBoxAlertSeguridad"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
        }

        private void LtiNotificacion()
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer && soSecurityConfiguration.isLtiActive)
            {
                if (!Application.absoluteURL.Contains(soSecurityConfiguration.validLtiUrl))
                    BoxMessageManager.Instance.CreateBoxMessageMini(DiccionarioIdiomas.Instance.Traducir("MensajeUrlInvalida"));
                else
                {
                    string[] _parametrosLTI = Application.absoluteURL.Split('?');

                    if (_parametrosLTI.Length > 1)
                    {
                        string[] arrayParametros = _parametrosLTI[1].Split('&');
                        soSecurityData.aulaUserFirstName = WWW.UnEscapeURL(arrayParametros[0]);
                        soSecurityData.aulaUserLastName = "";
                        soSecurityData.aulaClassId = WWW.UnEscapeURL(arrayParametros[2]);
                        soSecurityData.aulaClassName = WWW.UnEscapeURL(arrayParametros[1]);
                        soSecurityData.aulaSchoolName = WWW.UnEscapeURL(arrayParametros[3]);
                        soSecurityData.ltiDatos = WWW.UnEscapeURL(arrayParametros[4]);
                        soSecurityData.aulaUsername = soSecurityData.aulaUserFirstName;
                        soSecurityConfiguration.isConectionActive = true;
                    }

                    ActivePanelLogin();
                }
            }
        }
        
#if UNITY_ANDROID
        private void ProcessDirectory(DirectoryInfo aDir)
        {
            var files = aDir.GetFiles().Where(f => f.Extension == ".xml").ToArray();

            foreach (var _fileName in files)
            {
                if (_fileName.Name.Equals("aula_conf.xml"))
                {
                    soSecurityConfiguration.fileName = _fileName.FullName;
                    break;
                }
            }

            if (!soSecurityConfiguration.fileName.Equals(""))
                OpenXML();
        }
#endif

        /// <summary>
        /// Abre el archivo xml y extra la url y la variable aula
        /// </summary>
        private void OpenXML()
        {
            try
            {
                XmlDocument newXml = new XmlDocument();
                newXml.Load(soSecurityConfiguration.fileName);
                var tmpNodoUrlAula = newXml.GetElementsByTagName("url_aula");
                soSecurityConfiguration.urlClassroom = tmpNodoUrlAula[0].InnerText;
                var tmpNodoModoAula = newXml.GetElementsByTagName("aula");
                soSecurityConfiguration.isModeClassroom = tmpNodoModoAula[0].InnerText.Equals("true");
                Debug.LogError("Read modo aula " + soSecurityConfiguration.isModeClassroom);
                Debug.LogError("Read url_aula " + soSecurityConfiguration.urlClassroom);
            }
            catch (Exception e)
            {
                Debug.Log("no existe el archivo alua_conf " + e); //efBoxMessageManager.CreateBoxMessageInfo("No se pudo encontrar el archivo de configuracion esta es la direccion de la busqueda \n"+fileName, "ACEPTAR");
            }
        }

        /// <summary>
        ///  entrega la ubicacion correcta del archivo de aula dependeiendo de la plataforma
        /// </summary>
        private void CargarExternalXML()
        {
            soSecurityConfiguration.fileName = "";
#if UNITY_IPHONE
		    soSecurityConfiguration.fileName = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		    OpenXML();
#elif UNITY_ANDROID
            try
            {
                var _dir = "/storage/emulated/0/";
                var _dir0 = "/storage/sdcard/";
                var _dir1 = "/storage/sdcard0/";
                var _dir2 = "/storage/sdcard1/";
                var _dirx = "/sdcard/";
                DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
                DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
                DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
                DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
                DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

                if (currentDirectory.Exists)
                    ProcessDirectory(currentDirectory);
                else if (currentDirectory_0.Exists)
                    ProcessDirectory(currentDirectory_0);
                else if (currentDirectory_1.Exists)
                    ProcessDirectory(currentDirectory_1);
                else if (currentDirectory_2.Exists)
                    ProcessDirectory(currentDirectory_2);
                else if (currentDirectory_x.Exists)
                    ProcessDirectory(currentDirectory_x);
            }
            catch (Exception error)
            {
                Debug.Log("no se encontro direccion viable : " + error);
            }

#elif UNITY_EDITOR
            soSecurityConfiguration.fileName = Application.dataPath + "/" + "../../aula_conf.xml";
            OpenXML();
#elif UNITY_STANDALONE_OSX
		    soSecurityConfiguration.fileName = Application.dataPath + "/" + "../../aula_conf.xml"; 
		    OpenXML();
#elif UNITY_STANDALONE_WIN
		    soSecurityConfiguration.fileName = Application.dataPath + "/" + "../../../../aula_conf.xml";
		    OpenXML();
#endif
        }

        //--------------------------------------------------------------------loguin-------------------------------------------------------------------------------

        /// <summary>
        /// encripta caracteres 
        /// </summary>
        private string Md5Sum(string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);

            // Convert the encrypted bytes back to a string (base 16)
            string hashString = "";

            for (int i = 0; i < hashBytes.Length; i++)
                hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');

            return hashString.PadLeft(32, '0');
        }

        /// <summary>
        /// ordena los datos para la consulta de loguin
        /// </summary>
        private void AulaLoginRequest()
        {
#if UNITY_WEBGL
            string _params = "{\"user\":\"" + soSecurityData.inputUser + "\",\"pass\":\"" + soSecurityData.inputPassword + "\"}";
#else
            string _params = "{\"user\":\"" + soSecurityData.inputUser + "\",\"pass\":\"" + Md5Sum(soSecurityData.inputPassword) + "\"}";
#endif
            byte[] bytesToEncode = Encoding.UTF8.GetBytes(_params);
            string encodedText = Convert.ToBase64String(bytesToEncode);
            string url_get_aula = soSecurityConfiguration.urlClassroom + "/externals/login?data=" + encodedText;
            Debug.Log("El direccion enviada = " + _params);
            WWW wwwAula = new WWW(url_get_aula);
            StartCoroutine(WaitForRequestAula(wwwAula));
        }

        private void CambioModoOffline()
        {
            PanelLogin2Campos.Instance.ShowPanel(false);
            PanelInterfazLogin4Campos.Instance.ShowPanel();
        }

        public void Cerrar()
        {
            Application.Quit();
        }

        #endregion
    }
}