﻿#pragma warning disable 0649
using NSCreacionPDF;
using NSTraduccionIdiomas;
using System.Collections.Generic;
using NSScriptableEvent;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace NSCuaderno
{
    public class Cuaderno : MonoBehaviour
    {
        #region members

        public TextMeshProUGUI textPregunta;

        public TMP_InputField inputFieldCuaderno;

        public TextMeshProUGUI textPaginaActual;

        public int numeroDeCaracteres;

        public string[] preguntasComplementariasSituacion;

        public Sprite[] botonEliminar;

        public Sprite[] botonHojaNUeva;

        private List<string> HojasNuevas;

        [SerializeField] private Button buttonEliminar;

        [SerializeField] private Button buttonNuevaHoja;

        private List<string> preguntasDeMas;

        private List<string> ListRespuestas;

        private int hojaActual;

        private int numeroPreguntas;

        private bool hanEscritoRespuestas;

        private int hojasEnMemoria = 4;
        //modificacion luis

        private bool startOrNot;
        //final modificacion Luis

        [SerializeField] private ScriptableEventArrayString seQuestionsReady;
        
        #endregion

        #region monoBehaviour

        private void Awake()
        {
            InitCuaderno();
            //mtdPasarInfoAlPdf();
        }

        // Update is called once per frame
        private void Update()
        {
            textPaginaActual.text = DiccionarioIdiomas.Instance.Traducir("textPagina") + (hojaActual + 1);

            if ((ListRespuestas.Count <= hojaActual + 1) && hojaActual == (ListRespuestas.Count - 1) + HojasNuevas.Count)
            {
                if (HojasNuevas.Count < hojasEnMemoria)
                {
                    buttonNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[1];
                    buttonNuevaHoja.enabled = true;
                }
            }
            else
            {
                buttonNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[0];
                buttonNuevaHoja.enabled = false;
            }

            if (ListRespuestas.Count <= hojaActual && HojasNuevas.Count >= 1)
            {
                buttonEliminar.GetComponent<Image>().sprite = botonEliminar[1];
                buttonEliminar.enabled = true;
            }

            else
            {
                buttonEliminar.GetComponent<Image>().sprite = botonEliminar[0];
                buttonEliminar.enabled = false;
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// funsion que inicialisa la lista de preguntas
        /// </summary>
        private void MtdInitRespuestas()
        {
            mtdllenarlista(preguntasComplementariasSituacion);
        }

        /// <summary>
        /// Inicializa la lista que contendra las respuestas del usuario
        /// </summary>
        /// <param name="preguntas"></param>
        private void mtdllenarlista(string[] preguntas)
        {
            ListRespuestas.Clear();
            numeroPreguntas = 0;
            for (int i = 0; i < preguntas.Length; i++)
            {
                numeroPreguntas++;
                ListRespuestas.Add("");
            }
        }

        /// <summary>
        /// pone el texto de respues correspondiente a cada pregunta 
        /// </summary>
        private void mtdActualisarRespuesta()
        {
            if (hojaActual < ListRespuestas.Count)
                inputFieldCuaderno.text = ListRespuestas[hojaActual];
            else
                inputFieldCuaderno.text = HojasNuevas[hojaActual - ListRespuestas.Count];
        }
        
        /// <summary>
        ///  pinta la hoja del inputFieldCuaderno actual con la pregunta correspondiente
        /// </summary>
        private void mtdPintarPregunta()
        {
            if (hojaActual < preguntasComplementariasSituacion.Length)
                textPregunta.text = DiccionarioIdiomas.Instance.Traducir(preguntasComplementariasSituacion[hojaActual]);
            else
                textPregunta.text = "";
        }

        #endregion

        #region public methods

        /// <summary>
        /// inicializa el inputFieldCuaderno 
        /// </summary>
        public void InitCuaderno()
        {
            if(startOrNot)
            {
                return;
            }
            HojasNuevas = new List<string>();
            numeroPreguntas = 0;
            hojaActual = 0;
            ListRespuestas = new List<string>();
            
            inputFieldCuaderno.text = "";
            MtdInitRespuestas();
            mtdPintarPregunta();
            startOrNot = true;
        }
        
        /// <summary>
        /// guarda cada vez que hay un cambio en el texto de respuesta 
        /// </summary>
        public void mtdguardarCambios()
        {
            int tamActual = inputFieldCuaderno.text.Length;
            int tamAnterior = 0;
            hanEscritoRespuestas = true;

            if (hojaActual < ListRespuestas.Count)
                tamAnterior = ListRespuestas[hojaActual].Length;

            if (hojaActual < ListRespuestas.Count)
            {
                if (tamActual != tamAnterior)
                    ListRespuestas[hojaActual] = inputFieldCuaderno.text;
            }
            else
                HojasNuevas[hojaActual - ListRespuestas.Count] = inputFieldCuaderno.text;
        }
        
        public void btnAdelante()
        {
            if (numeroPreguntas + HojasNuevas.Count - 1 == hojaActual)
                Debug.Log("no hay mas hojas");
            else
            {
                hojaActual++;
                mtdPintarPregunta();
                mtdActualisarRespuesta();
            }
        }

        public void btnAtras()
        {
            if (0 == hojaActual)
                Debug.Log("no hay mas hojas");
            else
            {
                hojaActual = hojaActual - 1;
                mtdPintarPregunta();
                mtdActualisarRespuesta();
            }
        }
        
        public void reiniciarCuaderno()
        {
            inputFieldCuaderno.text = "";
            MtdInitRespuestas();
            HojasNuevas.Clear();
        }
        

        /// <summary>
        /// metodo que pasa los valoresd de las preguntas al pdf 
        /// </summary>
        public void mtdPasarInfoAlPdf()
        {
            InitCuaderno();
            bool HayHojasCreadas = false;
            string[] PregRespuesta;

            if (HojasNuevas.Count > 0)
            {
                PregRespuesta = new string[ListRespuestas.Count + HojasNuevas.Count];
                HayHojasCreadas = true;
            }
            else
                PregRespuesta = new string[ListRespuestas.Count];


            for (int i = 0; i < ListRespuestas.Count; i++)
                PregRespuesta[i] = DiccionarioIdiomas.Instance.Traducir(preguntasComplementariasSituacion[i]) + "\n" + ListRespuestas[i];

            if (HayHojasCreadas)
            {
                for (int i = 0; i < HojasNuevas.Count; i++)
                    PregRespuesta[i + ListRespuestas.Count] = HojasNuevas[i];
            }

            seQuestionsReady.ExecuteEvent(PregRespuesta);
        }

        public void mtdCrearNuevaHojas()
        {
            string aux = "";
            HojasNuevas.Add(aux);
            hojaActual = HojasNuevas.Count + (ListRespuestas.Count - 1);
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }

        public void mtdEliminarHojaNueva()
        {
            HojasNuevas.RemoveAt(hojaActual - ListRespuestas.Count);
            hojaActual--;
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }
        #endregion
    }
    
}
