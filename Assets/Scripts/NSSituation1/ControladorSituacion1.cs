﻿#pragma warning disable 0649
using System;
using UnityEngine;

namespace NSSituacion1
{
    
    public class ControladorSituacion1 : MonoBehaviour
    {

    }

    [Serializable]
    public class Transformador
    {
        public float potenciaKVA;

        public Sprite spriteTransformador;
    }

    public enum EtapaSituacionActual
    {
        Null,
        VestirAvatar,
        ZonaSeguridad,
        MacroMedidor,
        VisualizacionCortaCircuitoMalo,
        SeleccionCortaCircutoMalo,
        Transformador
    }
}