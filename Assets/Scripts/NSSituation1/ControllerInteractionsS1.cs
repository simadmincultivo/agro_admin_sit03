﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using NSInterfaz;
using TMPro;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class ControllerInteractionsS1 : MonoBehaviour
{
    [Header("Referencia a scripts")]
    [SerializeField] private StructuresControll1 myStructureController;
    [SerializeField] private ControllArrowBack myControllArrowBack;
    [SerializeField] private ValuesOfProductManager priceProducts;
    [SerializeField] private InstantiateController myInstantiateControll;
    private RegistroProductoController myRegistroProductoController;

    [Header("Camara captura escenario")]
    [SerializeField] private GameObject cameraCaptureScenary;

    [Header("Edificios situacion1")]
    [SerializeField] private GameObject invernaderoContainer;
    [SerializeField] private GameObject hangarContainer;
    [SerializeField] private GameObject germinaderoContainer;

    [Header("Objetos para construccion de edificios")]
    [SerializeField] private GameObject[] structuresInvernadero;

    [Header("Objetos para instalar en hangar")]
    [SerializeField] private GameObject[] containersWater;
    [SerializeField] private GameObject[] objectsFertirriego;

    [Header("UI de situación 1")]
    [SerializeField] private TMP_Dropdown typeClimatization;
    [SerializeField] private TMP_Dropdown[] dropsResourceHuman;

    [Header("Referencia a zonas de estructuras de edificios")]
    [SerializeField] private GameObject[] containersZonesStructure;

    [SerializeField] private bool enablePaneSelectedBuilding =  true;

    [Header("Contenedor de objetos de tableta")]
    [SerializeField] private GameObject containerTablet;

    [Header("Referencia de canvas en escena")]
    [SerializeField] private GameObject containerPanels;

    [Header("Contenedor de aumentos de edificios")]
    [SerializeField] private GameObject[] containerIncreases;

    private GameObject invernaderoGO;
    private bool invernadero = true;
    private bool hangar;
    private bool germinador;
    private int structureSelected = 0;
    private int coverSelected = 0;
    private int typeVentilation = 0;
    private int climatizationPos = 0;
    private int countUnitysSelected = 0;
    private bool enviromentLight = true;
    private bool installSoilAnalyzer = true;
    private bool preparerSoil = false;
    private int containerWaterSelect;
    private bool cabezaDeRiego = true;
    private bool toggleFertirriego1 = true;
    private bool toggleFertirriego2;
    private int countJornales = 0;    

    private void Start()
    {
        PanelInterfazBienvenida2.Instance.ShowPanel(true);
    }

    //Función que detecta que tipo de edificio se eligio (invernadero, Hangar, Germinadero)
    public void BuildingSelected(int build)
    {
        switch(build)
        {
            case 0:
                if(invernadero)
                    invernadero = false;
                else
                    invernadero = true;
                break;
            case 1:
                if(hangar)
                    hangar = false;
                else
                    hangar = true;
                break;
            case 2:
                if(germinador)
                    germinador = false;
                else
                    germinador = true;
                break;
        }
    }

    //Función que detecta que tipo de estructura para el edificio se eligio (Capilla, Túnel, Elíptico)
    public void StructureForBuilding(int typeStructure)
    {
        structureSelected = typeStructure;

        
    }

    //Función que detecta que tipo de cubierta se eligio
    public void CoverForBuilding(int cover)
    {
        coverSelected = cover;
    }

    //Función que detecta que tipo de ventilación se eligio
    public void VentilationSelected(int typeSelect)
    {
        typeVentilation = typeSelect;
    }

    //Función que detecta que tipo de climatización se eligio
    public void TypeClimatizationSelected()
    {
        climatizationPos = typeClimatization.value;
    }

    //Función detecta la cantidad de Unidades de climatización elegidas
    public void CountUnitysClimatization(int countUnitys)
    {
        countUnitysSelected = countUnitys;
    }

    //Función que detecta si se desea iluminación artificial o no
    public void EnviromentLightSelected(int lightSelect)
    {
        switch(lightSelect)
        {
            case 0:
                enviromentLight = true;
                break;
            case 1:
                enviromentLight = false;
                break;
        }
    }

    //funcion que detecta si eligio preparación de suelos o analisis de suelos
    public void SoilsSelected(int optionSelect)
    {
        switch(optionSelect)
        {
            case 0:
                if(installSoilAnalyzer)
                    installSoilAnalyzer = false;
                else
                    installSoilAnalyzer = true;
                break;
            case 1:
                if(preparerSoil)
                    preparerSoil = false;
                else
                    preparerSoil = true;
                break;
        }
    }

    //Función que detecta cuales contenedores de agua se eligieron
    public void ContainerWater(int count)
    {
        switch(count)
        {
            case 0:
                containerWaterSelect = count;
                break;
            case 1:
                containerWaterSelect = count;
                break;
            case 2:
                containerWaterSelect = count;
                break;
        }
    }

    //Función que detecta que objetos de fertirriego se eligieron
    public void EnableObjectsFertirriego(int count)
    {
        switch(count)
        {
            case 0:
                if(toggleFertirriego1)
                    toggleFertirriego1 = false;
                else
                    toggleFertirriego1 = true;
                break;
            case 1:
                if(toggleFertirriego2)
                    toggleFertirriego2 = false;
                else
                    toggleFertirriego2 = true;
                break;
        }
    }

    public void CountJornalesOfGermination(int count)
    {
        countJornales = count;
    }

    //Función para los botons de aceptar (continuar) de cada panel
    public void ButtonAceptar(int currentPanel)
    {
        string tagStructureDoor = DiccionarioIdiomas.Instance.Traducir("EstructurasPuertas");
        string tagTypeStructureDoor = DiccionarioIdiomas.Instance.Traducir("EstructurasPuertasTipo");
        string tagNameContainerWater = DiccionarioIdiomas.Instance.Traducir("NombreTanqueAgua");
        string tagNameUnidad = DiccionarioIdiomas.Instance.Traducir("TextUnidad");
        string tagNameFertirriego = DiccionarioIdiomas.Instance.Traducir("NombreFertirriego");
        string tagNameVentilacion = DiccionarioIdiomas.Instance.Traducir("NombreVentilacion");
        string tagNameRefrigeration = DiccionarioIdiomas.Instance.Traducir("NombreRefrigeracion");
        string tagNameEnviromentalControl = DiccionarioIdiomas.Instance.Traducir("NombreControlAmbiental");
        string tagNameSoil = DiccionarioIdiomas.Instance.Traducir("NombrePreparacionSuelos");
        string tagNameJornal = DiccionarioIdiomas.Instance.Traducir("NombreJornal");
        string tagNameServiceGerminator = DiccionarioIdiomas.Instance.Traducir("NombreJornalGerminadero");
        string tagNameHole = DiccionarioIdiomas.Instance.Traducir("NombreAhoyado");
        string tagNameOrganicMaterial = DiccionarioIdiomas.Instance.Traducir("TextMateriaOrganicaP2");
        string tagNameTutored = DiccionarioIdiomas.Instance.Traducir("NombreTutorado");
        string tagNameSowing = DiccionarioIdiomas.Instance.Traducir("NombreSiembra");

        switch(currentPanel)
        {
            case 0:// Boton aceptar del panel elección de edificio
                if(invernadero || hangar || germinador)
                {
                    PanelParametrosSimulacion.Instance.ShowPanel(false);
                    enablePaneSelectedBuilding = false;
                    if(!invernadero)
                    {
                        if(hangar)
                            hangarContainer.SetActive(true);

                        if(germinador)
                            germinaderoContainer.SetActive(true);
                    }else
                    {
                        PanelParametrosInvernadero.Instance.ShowPanel();
                    }
                }
                break;

            case 1: //Boton aceptar del panel elección de structura y cubierta
                PanelParametrosInvernadero.Instance.ShowPanel(false);
                if(hangar)
                {
                    hangarContainer.SetActive(true);
                }

                if(invernadero)
                {
                    invernaderoContainer.SetActive(true);
                    invernaderoGO = structuresInvernadero[structureSelected].transform.GetChild(coverSelected).gameObject;
                    invernaderoGO.SetActive(true);

                    if(structureSelected != 2)
                    {
                        containersZonesStructure[0].SetActive(true);
                        containersZonesStructure[1].SetActive(false);
                    }
                    else
                    {
                        containersZonesStructure[0].SetActive(false);
                        containersZonesStructure[1].SetActive(true);
                    }
                }

                if(germinador)
                {
                    germinaderoContainer.SetActive(true);
                }
                
                myInstantiateControll.InstanceProductInTablet("GreenHouse", "edificio" , tagStructureDoor, tagTypeStructureDoor, 1, (priceProducts.valuesStructureAndDoors[structureSelected] + priceProducts.valuesCubierta[coverSelected]));

                break;
            
            case 11://Boton aceptar de la primera interacción con el hangar contenedores de agua
                for(int i = 0; i < 3; i++)
                {
                    if(containersWater[i].activeSelf)
                    {
                        containersWater[i].SetActive(false);
                    }
                }
                    
                containersWater[containerWaterSelect].SetActive(true);
                string name0 = "TanqueAgua";
                PanelAlmacenamientoAgua.Instance.ShowPanel(false);
                myStructureController.ActivateActiveZone("ZoneContainerWater", false);
                
                myInstantiateControll.InstanceProductInTablet("Hangar", name0, tagNameContainerWater, tagNameUnidad, 1, priceProducts.valuesTanquesAgua[containerWaterSelect]);
                break;

            case 12://Boton aceptar de la segunda interacción con el hangar objetos fertirriego
                int count2 = 1;
                string namef = "Fertirriego";
                for(int i = 0; i < 3; i++)
                {
                    if(objectsFertirriego[i].activeSelf)
                        objectsFertirriego[i].SetActive(false);
                }

                
                if(toggleFertirriego1 && toggleFertirriego2)
                {
                    objectsFertirriego[2].SetActive(true);                    
                    myInstantiateControll.InstanceProductInTablet("Hangar", namef , tagNameFertirriego, tagNameUnidad, count2, (priceProducts.valuesFertirriego[0] + priceProducts.valuesFertirriego[1]));
                }
                else if(toggleFertirriego1)
                {
                    objectsFertirriego[0].SetActive(true);
                    myInstantiateControll.InstanceProductInTablet("Hangar", namef ,tagNameFertirriego, tagNameUnidad, count2, priceProducts.valuesFertirriego[0]);
                }
                else if(toggleFertirriego2)
                {
                    objectsFertirriego[1].SetActive(true);
                    myInstantiateControll.InstanceProductInTablet("Hangar", namef ,tagNameFertirriego, tagNameUnidad, count2, priceProducts.valuesFertirriego[1]);
                }
                PanelParametrosFertirrigacion.Instance.ShowPanel(false);
                myStructureController.ActivateActiveZone("ZoneFertirriego", false);
                
                break;
            case 21: //Boton aceptar de la primer interacción con el invernadero ventilación
                    for(int i = 0; i < 2; i++)
                    {
                        if(invernaderoGO.transform.GetChild(0).transform.GetChild(i).gameObject.activeSelf)
                        {
                            invernaderoGO.transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(false);
                            invernaderoGO.transform.GetChild(1).transform.GetChild(i).gameObject.SetActive(true);
                        }
                    }

                    PanelParametrosVentilacion.Instance.ShowPanel(false);
                    invernaderoGO.transform.GetChild(0).transform.GetChild(typeVentilation).gameObject.SetActive(true);
                    invernaderoGO.transform.GetChild(1).transform.GetChild(typeVentilation).gameObject.SetActive(false);
                    string name1 = "Ventilacion";
                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name1, tagNameVentilacion, tagTypeStructureDoor, 1, priceProducts.valuesVentilacion[typeVentilation]);
                break;
            case 22://Boton aceptar de la segunda interacción con el invernadero climatización
                PanelParametrosClimatizacion.Instance.ShowPanel(false);
                for(int i = 0; i < 3; i++)
                {
                    if(invernaderoContainer.transform.GetChild(3).transform.GetChild(i).gameObject.activeSelf)
                        invernaderoContainer.transform.GetChild(3).transform.GetChild(i).gameObject.SetActive(false);
                    if(invernaderoContainer.transform.GetChild(4).transform.GetChild(i).gameObject.activeSelf)
                        invernaderoContainer.transform.GetChild(4).transform.GetChild(i).gameObject.SetActive(false);
                }

                string name2 = "Climatizacion";
                if(climatizationPos == 0)
                {
                    for(int i = 0; i <= countUnitysSelected; i++)
                        invernaderoContainer.transform.GetChild(3).transform.GetChild(i).gameObject.SetActive(true);

                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name2, tagNameRefrigeration, tagNameUnidad, (countUnitysSelected + 1), priceProducts.valuesRefrigeracion[climatizationPos]);
                }
                else if(climatizationPos == 1)
                {
                    for(int i = 0; i <= countUnitysSelected; i++)
                        invernaderoContainer.transform.GetChild(4).transform.GetChild(i).gameObject.SetActive(true);

                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name2, tagNameRefrigeration, tagNameUnidad, (countUnitysSelected + 1), priceProducts.valuesRefrigeracion[climatizationPos]);
                }
                break;
            case 23://Botton aceptar de la tercera interacción con el invernadero luz ambiente
                PanelParametrosIluminacion.Instance.ShowPanel(false);
                if(structuresInvernadero[structureSelected].transform.GetChild(4).gameObject.activeSelf)
                    structuresInvernadero[structureSelected].transform.GetChild(4).gameObject.SetActive(false);
                
                string name4 = "LuzAmbiente";
                if(enviromentLight)
                {
                    structuresInvernadero[structureSelected].transform.GetChild(4).gameObject.SetActive(true);
                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name4, tagNameEnviromentalControl, tagNameUnidad, 1, priceProducts.valueControlAmbiental);
                }
                else
                {
                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name4, tagNameEnviromentalControl, tagNameUnidad, 1, 0);
                }
                break;
            case 24://Boton aceptar de la cuarta interacción con el invernadero suelos
                if(invernaderoGO.transform.GetChild(2).gameObject.activeSelf)
                    invernaderoGO.transform.GetChild(2).gameObject.SetActive(false);
                
                myStructureController.analyzerSoil = false;
                int count3 = 0;
                
                if(installSoilAnalyzer)
                {
                    PanelPreparacionSuelo.Instance.ShowPanel(false);
                    myStructureController.analyzerSoil = true;
                    count3++;
                }

                if(preparerSoil) 
                {
                    PanelPreparacionSuelo.Instance.ShowPanel(false);
                    invernaderoGO.transform.GetChild(2).gameObject.SetActive(true);
                    count3++;
                }
                
                string name5 = "Suelos";
                if(count3 <= 1)
                {
                    if(installSoilAnalyzer)
                        myInstantiateControll.InstanceProductInTablet("GreenHouse", name5, tagNameSoil, tagNameUnidad, 1, priceProducts.valuesAnalisisSuelo[0]);
                    else
                        myInstantiateControll.InstanceProductInTablet("GreenHouse", name5, tagNameSoil, tagNameUnidad, 1, priceProducts.valuesAnalisisSuelo[1]);
                }
                else
                {
                    myInstantiateControll.InstanceProductInTablet("GreenHouse", name5, tagNameSoil, tagNameUnidad, 1, (priceProducts.valuesAnalisisSuelo[0] + priceProducts.valuesAnalisisSuelo[1]));
                }

                break;
            case 30://Boton aceptar de la primera interacción con germinadero jornales para plantulas
                PanelRecursoHumano2.Instance.ShowPanel(false);
                myInstantiateControll.InstanceProductInTablet("Germinator", "ServiceGerminador", tagNameServiceGerminator, tagNameJornal, countJornales, priceProducts.priceJornal);
                break;
            case 31:// Boton aceptar de recursos humanos practica 1
                PanelRecursoHumano1.Instance.ShowPanel(false);
                int valueJ1 = int.Parse(dropsResourceHuman[0].captionText.text);
                int valueJ2 = int.Parse(dropsResourceHuman[1].captionText.text);
                int valueJ3 = int.Parse(dropsResourceHuman[2].captionText.text);
                int valueJ4 = int.Parse(dropsResourceHuman[3].captionText.text);
                myInstantiateControll.InstanceProductInTablet("GreenHouse", "RecursoHumanoP1G1", tagNameHole, tagNameJornal, valueJ1, priceProducts.priceJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouse", "RecursoHumanoP1G2", tagNameOrganicMaterial, tagNameJornal, valueJ2, priceProducts.priceJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouse", "RecursoHumanoP1G3", tagNameTutored, tagNameJornal, valueJ3, priceProducts.priceJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouse", "RecursoHumanoP1G4", tagNameSowing, tagNameJornal, valueJ4, priceProducts.priceJornal);
                break;
        }
    }

    public void ButtonRestarPractice()
    {
        RestarPractice();
    }

    public void ButtonResourceHuman()
    {
        if(myControllArrowBack.selectedGerminadero)
        {
            PanelRecursoHumano2.Instance.ShowPanel();
        }
        else 
        {
            PanelRecursoHumano1.Instance.ShowPanel();
        }
    }

    public void ScreenShootScenary()
    {
        StartCoroutine(ScreenShootScenaryCorutin());
    }

    IEnumerator ScreenShootScenaryCorutin()
    {
        cameraCaptureScenary.SetActive(true);
        yield return new WaitForEndOfFrame();
        cameraCaptureScenary.SetActive(false);
    }

    void RestarPractice()
    {
        for(int i = 0; i < containerTablet.transform.childCount; i++)
        {
            GameObject auxGO = containerTablet.transform.GetChild(i).gameObject;
            myRegistroProductoController = auxGO.GetComponent<RegistroProductoController>();
            
            float priceToRemove = 0;
            float lastPrice = float.Parse(auxGO.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text);
            float lastCount = float.Parse(auxGO.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text);
            priceToRemove = lastPrice * lastCount;
            myInstantiateControll.RemovePriceToCorrectZone(auxGO.tag, priceToRemove);
            myInstantiateControll.sumTotal.Remove(priceToRemove);
            myInstantiateControll.RealiceSumOfPrice();
            myRegistroProductoController.DeleteObjectInScene();
            Destroy(auxGO);
        }

        for(int j = 0; j < containerPanels.transform.childCount; j++)
        {
            containerPanels.transform.GetChild(j).gameObject.SetActive(false);
        }

        for(int k = 0; k < containerIncreases.Length; k++)
        {
            containerIncreases[k].SetActive(false);
        }

        invernaderoContainer.SetActive(false);
        hangarContainer.SetActive(false);
        germinaderoContainer.SetActive(false);
        PanelParametrosSimulacion.Instance.ShowPanel(true);
        myControllArrowBack.UpdateActualStateAndActiveZones(1);
    }
}