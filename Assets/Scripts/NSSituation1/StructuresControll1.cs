﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;
using NSActiveZone;

public class StructuresControll1 : AbstractObject2D
{
    public GameObject AssemblyComponentsHangar;
    public bool analyzerSoil;

    [SerializeField] private ControllArrowBack myControllArrowBack;
    [SerializeField] private GameObject backgroundGerminadero;

    void Start()
    {
        if(transform.gameObject.activeSelf)
        {
            actualStageIndex = 1;
            ActivateAllActiveZonesOfActualStage(true);
        }
    }

    public override void OnMoving()
    {
        
    }

    public override void OnTab()
    {
        
    }

    public override void OnTabOverActiveZone(string argNameActiveZone)
    {
        if(argNameActiveZone.Equals("ZoneV1") || argNameActiveZone.Equals("ZoneV2") || argNameActiveZone.Equals("ZoneV3") || argNameActiveZone.Equals("ZoneV4") || argNameActiveZone.Equals("ZoneV5") || argNameActiveZone.Equals("ZoneV6")
        || argNameActiveZone.Equals("ZoneV1T") || argNameActiveZone.Equals("ZoneV2T") || argNameActiveZone.Equals("ZoneV3T") || argNameActiveZone.Equals("ZoneV4T") || argNameActiveZone.Equals("ZoneV5T") || argNameActiveZone.Equals("ZoneV6T"))
        {
            PanelParametrosVentilacion.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneClimatizacion1") || argNameActiveZone.Equals("ZoneClimatizacion2") || argNameActiveZone.Equals("ZoneClimatizacion3"))
        {
            PanelParametrosClimatizacion.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneIluminacion"))
        {
            PanelParametrosIluminacion.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneSuelo1"))
        {
            PanelPreparacionSuelo.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneSuelo2"))
        {
            if(analyzerSoil)
            {
                PanelAnalyzerSoil.Instance.ShowPanel();
            }
        }
        else if(argNameActiveZone.Equals("ZoneHangar"))
        {
            DisableZones(1);
            myControllArrowBack.selectedHangar = true;
            AssemblyComponentsHangar.SetActive(true);
            UpdateActualStateAndActiveZones(2);
        }
        else if(argNameActiveZone.Equals("ZoneContainerWater"))
        {
            PanelAlmacenamientoAgua.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneFertirriego"))
        {
            PanelParametrosFertirrigacion.Instance.ShowPanel();
        }
        else if(argNameActiveZone.Equals("ZoneGerminadero"))
        {
            DisableZones(1);
            backgroundGerminadero.SetActive(true);
            myControllArrowBack.selectedGerminadero = true;
            PanelRecursoHumano2.Instance.ShowPanel();
        }
    }

    protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
    {
        
    }

    protected override void OnReleaseObjectOverNothing()
    {
        
    }

    public void UpdateActualStateAndActiveZones(int state)
    {
        ActualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(true);
    }

    void DisableZones(int state)
    {
        actualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(false);
    }
}
