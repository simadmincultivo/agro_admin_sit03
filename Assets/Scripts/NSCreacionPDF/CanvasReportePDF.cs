﻿using NSCalificacionSituacion;
using NSSeguridad;
using System;
using System.Collections;
using System.Collections.Generic;
using NSScriptableEvent;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.UI;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase encargada de capturar en imagen todas las hojas del PDF
    /// </summary>
    [RequireComponent(typeof(CreateSendPdfArchive), typeof(ControladorValoresPDF))]
    public class CanvasReportePDF : MonoBehaviour
    {
        #region members

        [Tooltip("Events")]
        [SerializeField] private ScriptableEvent seCreatePDFReport;
        
        /// <summary>
        /// Camara que hace la captura
        /// </summary>
        private Camera refCameraCapturePDF;

        /// <summary>
        /// Render texture en donde quedan capturas las imagenes de cada hoja
        /// </summary>
        private RenderTexture renderTexturePDF;

        /// <summary>
        /// Puntero a la courutina que captura las imagenes del PDF
        /// </summary>
        private IEnumerator couCapturarImagenPDF;

        /// <summary>
        /// Array de texturas que son las imagenes de las hojas capturadas
        /// </summary>
        private Texture2D[] capturaHojasPdf;

        /// <summary>
        /// referencia a la clase que gestiona el envio del PDF
        /// </summary>
        private CreateSendPdfArchive refCreateSendPdfArchive;

        /// <summary>
        /// referencia a la clase que asigna todos los valores al PDF, nombre de usuario, nombre simulador etc
        /// </summary>
        [Header("Datos adicionales"), Space(10)]
        private ControladorValoresPDF refControladorValoresPDF;

        /// <summary>
        /// string del tiempo que se demoro el usuario en la situacion
        /// </summary>
        private string tiempoSituacion;
        #endregion

        #region mono behaviour

        private void Awake()
        {
            refCreateSendPdfArchive = GetComponent<CreateSendPdfArchive>();
            refControladorValoresPDF = GetComponent<ControladorValoresPDF>();
            InitCreationPDF();
        }

        private void OnEnable()
        {
            seCreatePDFReport.Subscribe(CrearReportePDF);
        }

        private void OnDisable()
        {
            seCreatePDFReport.Unsubscribe(CrearReportePDF);
        }
        #endregion
        

        #region private methods
        
        /// <summary>
        /// Crea los objetos y variables necesarias para que la captura del PDF funcione
        /// </summary>
        private void InitCreationPDF()
        {
            renderTexturePDF = new RenderTexture(825, 1080, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            
            var tmpNewCamera = new GameObject("CameraCapturePDF");
            refCameraCapturePDF = tmpNewCamera.AddComponent<Camera>();
            refCameraCapturePDF.orthographic = true;
            refCameraCapturePDF.orthographicSize = 538f;
            refCameraCapturePDF.transform.SetParent(transform);
            refCameraCapturePDF.cullingMask = LayerMask.GetMask("CanvasPdfReport");
            refCameraCapturePDF.transform.position = new Vector3(0, 0, -10f);
            refCameraCapturePDF.gameObject.SetActive(false);
            refCameraCapturePDF.targetTexture = renderTexturePDF;
        }
        #endregion

        #region public methods

        /// <summary>
        /// Metodo que asigna todos los valores al PDF y ejecuta la courutina para capturar todas las hojas del PDF
        /// </summary>
        private void CrearReportePDF()
        {
            Debug.Log("CapturarImagenesPDF");
            refControladorValoresPDF.SetValuesOnPDF();

            if (couCapturarImagenPDF != null)
                StopCoroutine(couCapturarImagenPDF);
            
            couCapturarImagenPDF = CouCapturarImagePDF();
            StartCoroutine(couCapturarImagenPDF);
        }

        #endregion

        #region courutines

        /// <summary>
        /// Courutina que activa cada hoja del PDF para tomarle una captura
        /// </summary>
        private IEnumerator CouCapturarImagePDF()
        {
            var tmpPagesQuantity = transform.childCount;
            
            capturaHojasPdf = new Texture2D[tmpPagesQuantity - 1];
            refCameraCapturePDF.gameObject.SetActive(true);

            yield return null;

            for (int i = 0; i < tmpPagesQuantity - 1; i++)
            {
                var tmpGameObjectPanelHoja = transform.GetChild(i).gameObject;
                tmpGameObjectPanelHoja.SetActive(true);
                yield return new WaitForEndOfFrame();
                refCameraCapturePDF.Render();
                RenderTexture.active = renderTexturePDF;
                var tmpTexture2D = new Texture2D(825, 1080, TextureFormat.ARGB32, false, false);
                tmpTexture2D.ReadPixels(new Rect(0, 0, 825, 1080), 0, 0);
                tmpTexture2D.Apply();
                capturaHojasPdf[i] = tmpTexture2D;
                RenderTexture.active = null;
                tmpGameObjectPanelHoja.SetActive(false);
            }

            refCameraCapturePDF.gameObject.SetActive(false);
            refCreateSendPdfArchive.CrearReportePdf(capturaHojasPdf);
        }
        #endregion
    }
}