﻿#pragma warning disable
using System;
using System.Globalization;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace NSScore
{
    public class ScoreController : MonoBehaviour
    {
        #region members

        private XmlDocument xmlDocumentAula;


        private float minScoreValue;

        private float maxScoreValue;

        private QualificationElement[] arrayScoresAlphabetic;

        private string urlScore;

        private TypeScore typeScore = TypeScore.None;
        
        private XmlNode scoreTypeXmlNode;

        [SerializeField] private TypeScoreDefault typeScoreDefault = TypeScoreDefault.Numeric;
        
        [SerializeField] private float[] GlobalNumericScore = {0f, 5f};

        [SerializeField] private QualificationElement[] GlobalAlphabeticalScore =
        {
            new QualificationElement{name = "A", maxValue = 1},
            new QualificationElement{name = "B", maxValue = 0.9f},
            new QualificationElement{name = "C", maxValue = 0.8f},
            new QualificationElement{name = "D", maxValue = 0.7f},
            new QualificationElement{name = "F", maxValue = 0.6f}
        };
        
        [SerializeField] private float GlobalMinimalSuccessScore = 0.6f;

        private UnityEvent OnScoreSuccess;

        private UnityEvent OnScoreFailed;
        #endregion

        #region properties

        public XmlDocument XMLDocumentAula
        {
            set
            {
                xmlDocumentAula = value;
                
                if (xmlDocumentAula != null)
                    LoadScoreConfiguration();
            }
        }
        #endregion

        #region methods

        public void LoadScoreConfiguration()
        {
            scoreTypeXmlNode = xmlDocumentAula.SelectSingleNode("/data/general/score");

            if (scoreTypeXmlNode != null)
            {
                GlobalMinimalSuccessScore = float.Parse(scoreTypeXmlNode.Attributes["minimalSuccessScore"].Value);
                LoadScoreNumberRange();
                LoadScoreAlphabeticRange();
            }
            else
                Debug.LogWarning("El nodo score no existe en el archivo aula XML", this);
        }

        private void LoadScoreNumberRange()
        {
            var tmpNumericNode = scoreTypeXmlNode.SelectSingleNode("numeric");

            if (tmpNumericNode.Attributes["selected"].Value == "true")
            {
                typeScore = TypeScore.Numeric;
                minScoreValue = float.Parse(tmpNumericNode.ChildNodes[0].InnerText);
                maxScoreValue = float.Parse(tmpNumericNode.ChildNodes[1].InnerText);
            }
        }

        private void LoadScoreAlphabeticRange()
        {
            var tmpAlphabeticNode = scoreTypeXmlNode.SelectSingleNode("alphabetical");

            if (tmpAlphabeticNode.Attributes["selected"].Value == "true")
            {
                typeScore = TypeScore.Alphabetic;
                arrayScoresAlphabetic = new QualificationElement[tmpAlphabeticNode.ChildNodes.Count];

                for (int i = 0; i < tmpAlphabeticNode.ChildNodes.Count; i++)
                {
                    arrayScoresAlphabetic[i].name = tmpAlphabeticNode.ChildNodes[i].InnerText;
                    arrayScoresAlphabetic[i].maxValue = float.Parse(tmpAlphabeticNode.ChildNodes[i].Attributes["max"].Value);
                }
            }
        }

        public string GetScoreFromFactor01(float argFactorScore01)
        {
            var tmpFinalScore = string.Empty;

            if (typeScore == TypeScore.Numeric)
                tmpFinalScore = ComputeFinalScoreWithNumbersRange(argFactorScore01);
            else if (typeScore == TypeScore.Alphabetic)
                tmpFinalScore = ComputeFinalScoreWithAlphanumeric(argFactorScore01);
            else
                tmpFinalScore = ComputeFinalScoreWithDefaultRange(argFactorScore01);
            
            ExecuteEventOfScoreSuccess(argFactorScore01);
            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithNumbersRange(float argFactorScore01)
        {
            var tmpScoreScaled = argFactorScore01 * (maxScoreValue - minScoreValue) + minScoreValue;
            tmpScoreScaled = Mathf.Round(tmpScoreScaled * 10f) / 10f;
            
            var tmpFinalScore = tmpScoreScaled.ToString(CultureInfo.InvariantCulture);
            tmpFinalScore += "/" + maxScoreValue;
            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithAlphanumeric(float argFactorScore01)
        {
            string tmpFinalScore = String.Empty;

            for (int i = 0; i < arrayScoresAlphabetic.Length; i++)
                if (argFactorScore01 <= arrayScoresAlphabetic[i].maxValue)
                {
                    tmpFinalScore = arrayScoresAlphabetic[i].name;
                    break;
                }

            return tmpFinalScore;
        }

        private string ComputeFinalScoreWithDefaultRange(float argFactorScore01)
        {
            var tmpFinalScore = "";
            
            switch (typeScoreDefault)
            {
                case TypeScoreDefault.Numeric:
                    var tmpScoreScaled = argFactorScore01 * (GlobalNumericScore[1] - GlobalNumericScore[0]) + GlobalNumericScore[0];
                    tmpFinalScore = tmpScoreScaled.ToString("0.0", CultureInfo.InvariantCulture);
                    tmpFinalScore += "/" + GlobalNumericScore[1];
                    return tmpFinalScore;
                    
                case TypeScoreDefault.Alphabetic:

                    var tmpMinValue = Mathf.Infinity;
                    
                    for (int i = 0; i < GlobalAlphabeticalScore.Length; i++)
                        if (argFactorScore01 <= GlobalAlphabeticalScore[i].maxValue && GlobalAlphabeticalScore[i].maxValue <= tmpMinValue)
                        {
                            tmpMinValue = GlobalAlphabeticalScore[i].maxValue;
                            tmpFinalScore = GlobalAlphabeticalScore[i].name;
                        }

                    return tmpFinalScore;
            }

            return tmpFinalScore;
        }

        private void ExecuteEventOfScoreSuccess(float argFactorScore01)
        {
            //if (argFactorScore01 >= minimalSuccessScore)
                //OnScoreSuccess.Invoke();
            //else
                //OnScoreFailed.Invoke();
        }
        #endregion
    }

    public enum  TypeScore
    {
        None,
        Numeric,
        Alphabetic
    }
    public enum  TypeScoreDefault
    {
        Numeric,
        Alphabetic
    }

    [Serializable]
    public struct QualificationElement
    {
        public string name;

        public float maxValue;
    }
}