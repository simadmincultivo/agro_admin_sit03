﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

namespace NSTraduccionIdiomas
{
    [CreateAssetMenu(menuName = "NSTraduccionIdiomas/SOTranslateConfig", fileName = "SOTranslateConfig")]
    public class SOTranslateConfig : ScriptableObject
    {
        private Dictionary<string, string> dictionaryTranslate = new Dictionary<string, string>();

        public string idiomaActualSeleccionado;

        public string[] arrayIdiomas = new string[0];

        public void AddIdiom(string argNewIdiom)
        {
            var tmpListIdiom = arrayIdiomas.ToList();

            if (!tmpListIdiom.Contains(argNewIdiom))
            {
                tmpListIdiom.Add(argNewIdiom);
                arrayIdiomas = tmpListIdiom.ToArray();
                idiomaActualSeleccionado = argNewIdiom;
                CreateXmlFile();
            }
        }

        public void SelectIdiom(string argNameIdiom)
        {
            var tmpListIdiom = arrayIdiomas.ToList();

            if (tmpListIdiom.Contains(argNameIdiom))
            {
                idiomaActualSeleccionado = argNameIdiom;
                LoadAllTranslations();
            }
        }

        public void DeleteIdiom(string argNameIdiom)
        {
            var tmpListIdiom = arrayIdiomas.ToList();

            if (tmpListIdiom.Contains(argNameIdiom))
            {
                tmpListIdiom.Remove(argNameIdiom);
                idiomaActualSeleccionado = "";
                LoadAllTranslations();
            }
            
            arrayIdiomas = tmpListIdiom.ToArray();
        }

        private void CreateXmlFile()
        {
            var tmpPathFileXml = Application.streamingAssetsPath + "/NSSistemaTraduccion/tags" + idiomaActualSeleccionado + ".xml";

            var tmpXmlDocumentStructure = new XmlDocument();

            if (!File.Exists(tmpPathFileXml))
            {
                File.Create(tmpPathFileXml).Dispose();
                var tmpRoot = tmpXmlDocumentStructure.CreateElement("Tags");
                tmpXmlDocumentStructure.AppendChild(tmpRoot);
                tmpXmlDocumentStructure.Save(tmpPathFileXml);
            }

            AssetDatabase.Refresh();
        }

        public bool EditTranslation(string argTag, string argNewTranslation)
        {
            var tmpPathFileXml = Application.streamingAssetsPath + "/NSSistemaTraduccion/tags" + idiomaActualSeleccionado + ".xml";

            if (File.Exists(tmpPathFileXml))
            {
                var tmpXmlDocumentStructure = new XmlDocument();
                tmpXmlDocumentStructure.Load(tmpPathFileXml);

                var tmpTags = tmpXmlDocumentStructure.GetElementsByTagName(argTag);

                if (tmpTags.Count > 0)
                {
                    tmpTags[0].InnerXml = argNewTranslation;
                    dictionaryTranslate[argTag] = argNewTranslation;
                    tmpXmlDocumentStructure.Save(tmpPathFileXml);
                    return true;
                }

                var tmpNewTag = tmpXmlDocumentStructure.CreateElement(argTag);
                tmpNewTag.InnerXml = argNewTranslation;
                tmpXmlDocumentStructure.ChildNodes[0].AppendChild(tmpNewTag);
                tmpXmlDocumentStructure.Save(tmpPathFileXml);
            }

            return false;
        }

        public bool CreateTranslation(string argTag)
        {
            var tmpPathFileXml = Application.streamingAssetsPath + "/NSSistemaTraduccion/tags" + idiomaActualSeleccionado + ".xml";

            if (File.Exists(tmpPathFileXml))
            {
                var tmpXmlDocumentStructure = new XmlDocument();
                tmpXmlDocumentStructure.Load(tmpPathFileXml);

                var tmpTags = tmpXmlDocumentStructure.GetElementsByTagName(argTag);

                if (tmpTags.Count > 0)
                    return false;
                

                var tmpNewTag = tmpXmlDocumentStructure.CreateElement(argTag);
                tmpXmlDocumentStructure.ChildNodes[0].AppendChild(tmpNewTag);
                tmpXmlDocumentStructure.Save(tmpPathFileXml);
                dictionaryTranslate[argTag] = "";
            }

            return true;
        }

        public bool DeleteTranslation(string argTag)
        {
            var tmpPathFileXml = Application.streamingAssetsPath + "/NSSistemaTraduccion/tags" + idiomaActualSeleccionado + ".xml";

            if (File.Exists(tmpPathFileXml))
            {
                var tmpXmlDocumentStructure = new XmlDocument();
                tmpXmlDocumentStructure.Load(tmpPathFileXml);

                var tmpTags = tmpXmlDocumentStructure.GetElementsByTagName(argTag);

                if (tmpTags.Count > 0)
                {
                    tmpXmlDocumentStructure.RemoveChild(tmpTags[0]);
                    dictionaryTranslate.Remove(argTag);
                    tmpXmlDocumentStructure.Save(tmpPathFileXml);
                    return true;
                }

                tmpXmlDocumentStructure.Save(tmpPathFileXml);
            }

            return false;
        }

        public void LoadAllTranslations()
        {
            var tmpPathFileXml = Application.streamingAssetsPath + "/NSSistemaTraduccion/tags" + idiomaActualSeleccionado + ".xml";

            dictionaryTranslate.Clear();

            if (File.Exists(tmpPathFileXml))
            {
                var tmpXmlDocumentStructure = new XmlDocument();
                tmpXmlDocumentStructure.Load(tmpPathFileXml);

                var tmpChildNodes = tmpXmlDocumentStructure.ChildNodes[0].ChildNodes;

                for (int i = 0; i < tmpChildNodes.Count; i++)
                    dictionaryTranslate.Add(tmpChildNodes[i].Name, tmpChildNodes[i].InnerXml);

            }
            else
                dictionaryTranslate.Clear();
        }

        public string GetTranslation(string argTag)
        {
            if (!dictionaryTranslate.ContainsKey(argTag))
                dictionaryTranslate.Add(argTag, "");

            return dictionaryTranslate[argTag];
        }
    }
}