#pragma warning disable 0649
using NSAvancedUI;
using NSCreacionPDF;
using NSCalificacionSituacion;
using NSScriptableValues;
using NSSeguridad;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;

namespace NSAvancedUI
{
    public class PanelCalificacionFinal : AbstractSingletonPanelUIAnimation<PanelCalificacionFinal>
    {
        #region members

        [Header("Security Data")]
        [SerializeField] private SOSecurityData refSoSecurityData;
        
        [Header("Configuration")]
        [SerializeField] private TextMeshProUGUI textValorCalificacion;

        [SerializeField] private TextMeshProUGUI textNombreUsuario;

        [Tooltip("Frases de aliento")]
        [SerializeField] private TextMeshProUGUI textFelicitacion1;

        [SerializeField] private TextMeshProUGUI textFelicitacion2;

        [Header("Calificacion final")] 
        [SerializeField] private SOCalificacionSituacion soCalificacionSituacion;
        
        [SerializeField] private ValueFloat valCalificacionFinalSituacion;
        
        [SerializeField] private ValueString valCalificacionFinalRangoNumerosLetras;
        #endregion

        #region monoBehaviour

        private void OnEnable()
        {
            soCalificacionSituacion.CalcularCalificacionFinal();
            
            //Asignar textos calificacion
            if (valCalificacionFinalSituacion > 0.6f)
            {
                textFelicitacion1.text = DiccionarioIdiomas.Instance.Traducir("textFelicitacion1CalificacionMas60Porciento");
                textFelicitacion2.text = DiccionarioIdiomas.Instance.Traducir("textFelicitacion2CalificacionMas60Porciento");
            }
            else
            {
                textFelicitacion1.text = DiccionarioIdiomas.Instance.Traducir("textFelicitacion1CalificacionMenos60Porciento");
                textFelicitacion2.text = DiccionarioIdiomas.Instance.Traducir("textFelicitacion2CalificacionMenos60Porciento");
            }
            
            textNombreUsuario.text = refSoSecurityData.aulaUserFirstName;
            textValorCalificacion.text = valCalificacionFinalRangoNumerosLetras;
        }
        #endregion
    }
}