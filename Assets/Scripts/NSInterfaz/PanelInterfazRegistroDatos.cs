﻿#pragma warning disable 0649
using NSAvancedUI;
using NSBoxMessage;
using NSCreacionPDF;
using NSCalificacionSituacion;
using NSInterfazAvanzada;
using NSSituacion1;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class PanelInterfazRegistroDatos : AbstractSingletonPanelUIAnimation<PanelInterfazRegistroDatos>
    {
        #region members

        [SerializeField] private ControladorSituacion1 refControladorSituacion1;

        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField] private ControladorValoresPDF refControladorValoresPDF;

        [SerializeField] private Button buttonReporte;

        [SerializeField] private SOCalificacionSituacion refSoCalificacionSituacion;

        [SerializeField] private TMP_InputField inputCorrienteLP;

        [SerializeField] private TMP_InputField inputValorFusible;


        [SerializeField, Header("Corriente")] private TMP_InputField inputCorrrienteX1;

        [SerializeField] private TMP_InputField inputCorrrienteX2;

        [SerializeField] private TMP_InputField inputCorrrienteX3;


        [SerializeField, Header("Tension")] private TMP_InputField inputTensionX1;

        [SerializeField] private TMP_InputField inputTensionX2;

        [SerializeField] private TMP_InputField inputTensionX3;

        //calificacion
        private float calificacion;

        #endregion

        public void OnButtonValidar()
        {
            if (ValidarEmptyInputs())
            {
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeValidarFelicitaciones"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
            else
            {
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeValidarIncorrectos"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void OnButtonReporte()
        {
            if (ValidarDatos())
                BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeValidarCorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextReporte"), OnDatosCorrectos);
            else
            {
                BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeValidarIncorrectosReporte"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextReporte"), OnDatosCorrectos);
                refControladorDatosSesion.AddIntentos();
            }
        }

        public float GetFusibleValue()
        {
            /*if (inputValorFusible.text.Equals(""))
                return -1f;

            float tmpValueFusible;
            float.TryParse(inputValorFusible.text, out tmpValueFusible);

            if (tmpValueFusible < refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
            {
                SetInputIncorrecto(inputValorFusible);
                return 0;
            }

            if (tmpValueFusible == refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
                return 1;

            if (tmpValueFusible > refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
                return 2;*/

            return 0;
        }

        private void OnDatosCorrectos()
        {
            ShowPanel(false);
            refPanelInterfazEvaluacion.ShowPanel();
            //buttonReporte.interactable = true;
        }

        private bool ValidarDatos()
        {
            /*ResetInputsIncorrectos();

            var tmpFragmentoCalificacion = 1f / 7f;
            calificacion = 0;

            float tmpValueCorrienteLP;
            float.TryParse(inputCorrienteLP.text, out tmpValueCorrienteLP);

            var tmpCorrienteLPCalculate = (refControladorSituacion1._potenciaTransformadorActual / 23902.301f) * 1.45f;
            var tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueCorrienteLP - tmpCorrienteLPCalculate) <= tmpCorrienteLPCalculate * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputCorrienteLP);

            float tmpValueFusible;
            float.TryParse(inputValorFusible.text, out tmpValueFusible);

            if (tmpValueFusible == refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
                refSoCalificacionSituacion.AsignarCalificacionFusibleSeleccioando(1);
            else
            {
                refSoCalificacionSituacion.AsignarCalificacionFusibleSeleccioando(0);
                SetInputIncorrecto(inputValorFusible);
            }

            //corriente
            float tmpValueCorrienteX1;
            float.TryParse(inputCorrrienteX1.text, out tmpValueCorrienteX1);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueCorrienteX1 - refControladorSituacion1._corrienteCablesTransformador[0]) <= refControladorSituacion1._corrienteCablesTransformador[0] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputCorrrienteX1);

            float tmpValueCorrienteX2;
            float.TryParse(inputCorrrienteX2.text, out tmpValueCorrienteX2);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueCorrienteX2 - refControladorSituacion1._corrienteCablesTransformador[1]) <= refControladorSituacion1._corrienteCablesTransformador[1] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputCorrrienteX2);

            float tmpValueCorrienteX3;
            float.TryParse(inputCorrrienteX3.text, out tmpValueCorrienteX3);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueCorrienteX3 - refControladorSituacion1._corrienteCablesTransformador[2]) <= refControladorSituacion1._corrienteCablesTransformador[2] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputCorrrienteX3);

            //tension
            float tmpValueTensionX1;
            float.TryParse(inputTensionX1.text, out tmpValueTensionX1);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueTensionX1 - refControladorSituacion1._tensionFasePuntasPinza[0]) <= refControladorSituacion1._tensionFasePuntasPinza[0] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputTensionX1);

            float tmpValueTensionX2;
            float.TryParse(inputTensionX2.text, out tmpValueTensionX2);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueTensionX2 - refControladorSituacion1._tensionFasePuntasPinza[1]) <= refControladorSituacion1._tensionFasePuntasPinza[1] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputTensionX2);

            float tmpValueTensionX3;
            float.TryParse(inputTensionX3.text, out tmpValueTensionX3);

            tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValueTensionX3 - refControladorSituacion1._tensionFasePuntasPinza[2]) <= refControladorSituacion1._tensionFasePuntasPinza[2] * 0.02f;

            if (tmpDiferenceValueIsCorrect)
                calificacion += tmpFragmentoCalificacion;
            else
                SetInputIncorrecto(inputTensionX3);

            refSoCalificacionSituacion.AsignarCalificacionRegistroDatos(calificacion);
            Debug.Log("AsignarCalificacionRegistroDatos : " + calificacion);

            if (Mathf.Approximately(calificacion, 1f))
                return true;
*/
            return false;
        }

        private bool ValidarEmptyInputs()
        {
            var tmpEmptyInputs = 0;

            if (inputTensionX1.text.Equals(""))
                tmpEmptyInputs++;

            if (inputTensionX2.text.Equals(""))
                tmpEmptyInputs++;

            if (inputTensionX3.text.Equals(""))
                tmpEmptyInputs++;

            if (inputCorrrienteX1.text.Equals(""))
                tmpEmptyInputs++;

            if (inputCorrrienteX2.text.Equals(""))
                tmpEmptyInputs++;

            if (inputCorrrienteX3.text.Equals(""))
                tmpEmptyInputs++;

            if (inputValorFusible.text.Equals(""))
                tmpEmptyInputs++;

            if (inputCorrienteLP.text.Equals(""))
                tmpEmptyInputs++;

            return tmpEmptyInputs > 0;
        }

        private void ResetInputsIncorrectos()
        {
            SetInputIncorrecto(inputCorrienteLP, false);
            SetInputIncorrecto(inputValorFusible, false);

            SetInputIncorrecto(inputTensionX1, false);
            SetInputIncorrecto(inputTensionX2, false);
            SetInputIncorrecto(inputTensionX3, false);

            SetInputIncorrecto(inputCorrrienteX1, false);
            SetInputIncorrecto(inputCorrrienteX2, false);
            SetInputIncorrecto(inputCorrrienteX3, false);
        }

        private void SetInputIncorrecto(TMP_InputField argInputField, bool argIncorrecto = true)
        {
            argInputField.transform.Find("ImageBadInput").gameObject.SetActive(argIncorrecto);
        }
    }
}