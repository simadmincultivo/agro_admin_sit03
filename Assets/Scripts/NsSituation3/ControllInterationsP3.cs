﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using NSInterfaz;
using TMPro;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;
using NSSceneLoading;

public class ControllInterationsP3 : MonoBehaviour
{
    [Header("Referencias a scripts")]
    [SerializeField] private InstantiateController myInstanceControll;
    [SerializeField] private ValuesOfProductsManagerP3 priceProducts;
    [SerializeField] private ControllArrowBack myControllArrowBack;
    private RegistroProductoController myRegistroProductoController;

    [Header("Objetos de UI en paneles para cada registro")]
    [SerializeField] private TMP_Dropdown rhGreenHouse;
    [SerializeField] private TMP_Dropdown rhCollectionCenter;
    [SerializeField] private TMP_InputField countCanastillas;
    [SerializeField] private TMP_InputField countCanastillas2;
    [SerializeField] private TMP_InputField countBalanzas;
    [SerializeField] private TMP_Dropdown dropLimpieza;
    [SerializeField] private TMP_Dropdown dropEmpaque;
    [SerializeField] private TMP_Dropdown dropAdmin;
    [SerializeField] private TMP_Dropdown dropSecretaria;

    [Header("Objetos que se activan en escena")]
    [SerializeField] private GameObject balance;
    [SerializeField] private GameObject[] layettesForAnimation;
    [SerializeField] private GameObject layetteOne;
    [SerializeField] private GameObject peoples;

    [Header("Camara captura de escenario")]
    [SerializeField] private GameObject cameraCaptureScenary;

    [Header("Tiempo y texto de espera para desaparecer texto de confirmación")]
    [SerializeField] private float waitTimeConfirmation;

    [Header("Contenedor de objetos de tableta")]
    [SerializeField] private GameObject containerTablet;

    [Header("Referencia de canvas en escena")]
    [SerializeField] private GameObject containerPanels;

    [Header("Contenedor de aumentos")]
    [SerializeField] private GameObject[] containerIncreases;

    [SerializeField]
    private PanelBarraUsuario barraUsuario;
    [SerializeField]
    private PanelParametrosSimulacion panelDeInicioPracticas;


    private void Start()
    {

        if (PlayerPrefs.HasKey("intentos"))
        {
            barraUsuario.LoadAttempts(PlayerPrefs.GetInt("intentos"));
          //  StartCoroutine(activarmanelpractica());
            PlayerPrefs.DeleteKey("intentos");
        }
    }

    IEnumerator activarmanelpractica()
    {
        yield return new WaitForEndOfFrame();
        activarBienvenida();
        // panelDeInicioPracticas.ShowPanel(true);
    }

    public void activarBienvenida()
    {
        PanelInterfazBienvenida2.Instance.ShowPanel();
    }
    public void ButtonAceptar(int option)
    {
        string tagNameUnidad = DiccionarioIdiomas.Instance.Traducir("TextUnidad");
        string tagNameJornal = DiccionarioIdiomas.Instance.Traducir("NombreJornal");
        string tagNameRecolection = DiccionarioIdiomas.Instance.Traducir("TextRecoleccion");
        string tagNamePosRecolection = DiccionarioIdiomas.Instance.Traducir("TextPosCosecha");
        string tagNameCanastilla = DiccionarioIdiomas.Instance.Traducir("TextCanastilla");
        string tagNameCanastilla2 = DiccionarioIdiomas.Instance.Traducir("TextCanastillaEmpaque");
        string tagNameBalanza = DiccionarioIdiomas.Instance.Traducir("TextBalanza");
        string tagNameLimpieza = DiccionarioIdiomas.Instance.Traducir("TextFertilizacionP3");
        string tagNameSeleccionEmpaque = DiccionarioIdiomas.Instance.Traducir("TextSeleccionEmpaque");
        string tagNameMes = DiccionarioIdiomas.Instance.Traducir("TextMes");
        string tagNameAdmin = DiccionarioIdiomas.Instance.Traducir("TextAdministradorP3");
        string tagNameSecretaria = DiccionarioIdiomas.Instance.Traducir("TextSecretariaP3");
        switch (option)
        {
            case 0:
                PanelRecursoHumano1Situacion3.Instance.ShowPanel(false);
                peoples.SetActive(true);
                int value1 = int.Parse(rhGreenHouse.captionText.text);
                myInstanceControll.InstanceProductInTablet("GreenHouseP3", "RHGreenHouse", tagNameRecolection, tagNameJornal, value1, priceProducts.priceJornalRecoleccion);
                break;
            case 1:
                PanelRecursoHumano2Situacion3.Instance.ShowPanel(false);
                int value2 = int.Parse(rhCollectionCenter.captionText.text);
                myInstanceControll.InstanceProductInTablet("Storage2", "RGCollecntion", tagNamePosRecolection, tagNameJornal, value2, priceProducts.priceJornalPosCosecha);
                break;
            case 2:
                if(countCanastillas.text != "")
                {
                    int value3 = int.Parse(countCanastillas.text);
                    layetteOne.SetActive(true);
                    myInstanceControll.InstanceProductInTablet("Storage", "Canastillas", tagNameCanastilla, tagNameUnidad, value3, priceProducts.priceCanastilla);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 3:
                if(countCanastillas2.text != "")
                {
                    int value4 = int.Parse(countCanastillas2.text);
                    StartCoroutine(AnimationLayette());
                    myInstanceControll.InstanceProductInTablet("Selected", "Canastillas2", tagNameCanastilla2, tagNameUnidad, value4, priceProducts.priceCanastilla);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 4:
                if(countBalanzas.text != "")
                {
                    int value5 = int.Parse(countBalanzas.text);
                    balance.SetActive(true);
                    myInstanceControll.InstanceProductInTablet("Selected", "Balanza", tagNameBalanza, tagNameUnidad, value5, priceProducts.priceBalanza);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 5:
                PanelRecursoHumano3Situacion3.Instance.ShowPanel(false);
                int value6 = int.Parse(dropLimpieza.captionText.text);
                int value7 = int.Parse(dropEmpaque.captionText.text);
                myInstanceControll.InstanceProductInTablet("Selected2", "Limpieza", tagNameLimpieza, tagNameJornal, value6, priceProducts.priceJornalLimpieza);
                myInstanceControll.InstanceProductInTablet("Selected2", "Empaque", tagNameSeleccionEmpaque, tagNameJornal, value7, priceProducts.priceJornalSeleccionYEmpaque);
                break;
            case 6:
                PanelRecursoHumano4Situacion3.Instance.ShowPanel(false);
                int value8 = int.Parse(dropAdmin.captionText.text);
                int value9 = int.Parse(dropSecretaria.captionText.text);
                myInstanceControll.InstanceProductInTablet("Admin", "Admin", tagNameAdmin, tagNameMes, value8, priceProducts.priceJornalAdministrador);
                myInstanceControll.InstanceProductInTablet("Admin", "Secretaria", tagNameSecretaria, tagNameMes, value9, priceProducts.priceJornalSecretaria);
                break;
        }
    }

    IEnumerator AnimationLayette()
    {
        float timeEnable = 0.3f;
        layettesForAnimation[0].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[1].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[2].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[3].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[4].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[5].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[6].SetActive(true);
        yield return new WaitForSeconds(timeEnable);
        layettesForAnimation[7].SetActive(true);
    }

    public void ButtonRestart(string nameScene)
    {
        // SceneManager.UnloadSceneAsync(nameScene);
        // SceneManager.LoadScene(nameScene, LoadSceneMode.Additive);
        RestarPractice();
    }

    public void ScreenShootScenary()
    {
        StartCoroutine(ScreenShootScenaryCorutin());
    }

    IEnumerator ScreenShootScenaryCorutin()
    {
        cameraCaptureScenary.SetActive(true);
        yield return new WaitForEndOfFrame();
        cameraCaptureScenary.SetActive(false);
    }

    public void SaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        StartCoroutine(EventSaveConfirmation(textConfirmation));
    }

    IEnumerator EventSaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        textConfirmation.transform.gameObject.SetActive(true);
        yield return new WaitForSeconds(waitTimeConfirmation);
        textConfirmation.transform.gameObject.SetActive(false);
    }

    void RestarPractice()
    {

        PlayerPrefs.SetInt("intentos", barraUsuario.GetAttempts());

        SceneLoader.Instance.LoadScene("Practica3", true, SceneManager.GetActiveScene());
    }
}
