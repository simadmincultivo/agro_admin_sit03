﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValuesOfProductsManagerP3 : MonoBehaviour
{
    public int priceJornalRecoleccion;
    public int priceJornalPosCosecha;
    public int priceJornalLimpieza;
    public int priceJornalSeleccionYEmpaque;
    public int priceJornalAdministrador;
    public int priceJornalSecretaria;
    public int priceCanastilla;
    public int priceBalanza;

    [Header("Valores Aleatorios")]

    public float largoCultivo;
    public float anchoCultivo;
    public float distanciaCalleMayor;
    public float distanciaEntrePlantas;
    public float cantidadProduccidoPlanta;
    public float kilogrmosPorCanastilla;
    public float cantidadMalaCalidad;
    public float cantidadKilosProducidosCultivo;
    
    public float cantidadCanastillasAcopio;
    public float cantidadCanastillas;
    [System.NonSerialized] public int countPlantasMetroCuadrado;
    //[System.NonSerialized] public float countTomateRecolectado;
    [System.NonSerialized] public float countTomateAlmacenado;
    [System.NonSerialized] public int countDiasRecoleccion;
    [System.NonSerialized] public int countDiasAcopio;
    [System.NonSerialized] public int countDiasLimpieza;
    [System.NonSerialized] public int countDiasSeleccion;

    private float areaCultivo;
    private float densidadSiembra;
    

    private float[] floatForRandom = new float[]{20f, 20.5f, 21f, 21.5f, 22, 22.5f, 23f, 23.5f, 24f, 24.5f, 25f};
    private float[] distanciasDeSurcoCalleMayor = new float[] {1f, 1.1f, 1.2f, 1.3f, 1.4f};
    private float[] distanciasDeSurcoPlantas = new float[] {25.5f, 26f, 26.5f, 27f, 27.5f, 28f, 28.5f, 29f, 29.5f, 30f, 30.5f, 31f, 31.5f, 32f, 32.5f, 33f, 33.5f, 34f, 34.5f};
    private float[] produccionDePlanta = new float[] {3.6f, 3.7f, 3.8f, 3.9f, 4.0f, 4.1f, 4.2f, 4.3f, 4.4f, 4.5f, 4.6f, 4.7f, 4.8f, 4.9f, 5.0f, 5.1f, 5.2f, 5.3f, 5.4f};

    void Start()
    {
        SetValuesP3();
        CalcularValoresFijos();
    }

    void SetValuesP3()
    {
        largoCultivo = Random.Range(30, 41);
        anchoCultivo = Random.Range(6, 13);
        distanciaCalleMayor = distanciasDeSurcoCalleMayor[Random.Range(0, distanciasDeSurcoCalleMayor.Length)];
        distanciaEntrePlantas = distanciasDeSurcoPlantas[Random.Range(0, distanciasDeSurcoPlantas.Length)];
        cantidadProduccidoPlanta = produccionDePlanta[Random.Range(0, produccionDePlanta.Length)];
        countPlantasMetroCuadrado = Random.Range(4, 11);
        countTomateAlmacenado = floatForRandom[Random.Range(0, floatForRandom.Length)];
        kilogrmosPorCanastilla = countTomateAlmacenado;
        countDiasRecoleccion = Random.Range(8, 13);
        countDiasAcopio = Random.Range(8, 13);
        countDiasLimpieza = Random.Range(5, 9);
        countDiasSeleccion = Random.Range(5, 9);
    }

    void CalcularValoresFijos()
    {
        areaCultivo = anchoCultivo * largoCultivo;
        densidadSiembra = (areaCultivo / (distanciaCalleMayor * (distanciaEntrePlantas / 100)));
        float cantidadProducidaParcial = cantidadProduccidoPlanta * densidadSiembra;
        float cantidadProductoPerdido = (cantidadProducidaParcial / 100) * 5;
        cantidadKilosProducidosCultivo = cantidadProducidaParcial - cantidadProductoPerdido;
        cantidadCanastillasAcopio = cantidadProducidaParcial / kilogrmosPorCanastilla;
        cantidadCanastillas = cantidadKilosProducidosCultivo / kilogrmosPorCanastilla;
    }
}
