﻿using System;
using TMPro;
using UnityEngine;

public class CalculoCantidadProductosP3 : MonoBehaviour
{
   [Header("Variable calificación ventanas individuales")]
   public float calificacion1;

   [SerializeField] private ValuesOfProductsManagerP3 myValuesProductos;

   [Header("objectos y datos de invernadero")] [SerializeField]
   private TMP_Dropdown dropInvernadero;

   [Header("Objetos y datos de centro de acopio")] [SerializeField]
   private TMP_InputField fieldCantidadCanastillas;

   [SerializeField] private TMP_Dropdown dropRecursoHumanoAlmacenamiento;

   [Header("Objetos y datos de selección y empaque")] [SerializeField]
   private TMP_InputField fieldCantidadCanastillas2;

   [SerializeField] private TMP_InputField fieldBalanzas;
   [SerializeField] private TMP_Dropdown[] fieldsLimpiezaSeleccion;

   [Header("Objetos y datos de administración")] [SerializeField]
   private TMP_Dropdown[] fieldAdmin;

   [Header("Valor porcentaje error")] [SerializeField]
   private float valueError;

   private int cantidadJornalesAdmin;
   private float cantidadJornalesAlmacenamiento;
   private float cantidadJornalesInvernadero;
   private float cantidadJornalesLimpieza;
   private int cantidadJornalesSecretaria;
   private float cantidadJornalesSeleccion;

   private bool duplicateValue;
   private bool duplicateValue1;
   private bool duplicateValue2;
   private bool duplicateValue3;
   private bool duplicateValue4;
   private bool duplicateValue5;
   private bool duplicateValue6;

   private readonly float valueToDivideCalification = 1f / 7f;

   public void CalcularJornalesInvernadero()
   {
      cantidadJornalesInvernadero = CalculateJornales(2f);
      
      if(Math.Abs(int.Parse(dropInvernadero.captionText.text) - cantidadJornalesInvernadero) < 0.1f)
      {
         if(!duplicateValue)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue = true;
         }
      }
      else
      {
         duplicateValue = false;
      }

      Debug.Log("CalcularJornalesInvernadero = " + calificacion1);
   }

   public void VerificarCantidadCanastillas()
   {
      var aux = myValuesProductos.cantidadCanastillasAcopio;
      var valueUser = int.Parse(fieldCantidadCanastillas.text);
      if(valueUser >= aux / valueError && valueUser <= aux * valueError)
      {
         if(!duplicateValue1)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue1 = true;
         }
      }
      else
      {
         duplicateValue1 = false;
      }

      Debug.Log("VerificarCantidadCanastillas = " + calificacion1);
   }

   public void VerificarJornalesAlmacenamiento()
   {
      cantidadJornalesAlmacenamiento = CalculateJornales(2f);
      if(Math.Abs(int.Parse(dropRecursoHumanoAlmacenamiento.captionText.text) - cantidadJornalesAlmacenamiento) < 0.1f)
      {
         if(!duplicateValue2)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue2 = true;
         }
      }
      else
      {
         duplicateValue2 = false;
      }

      Debug.Log("VerificarJornalesAlmacenamiento  = " + calificacion1);
   }

   public void VerificarCantidadCanastillas2()
   {
      var aux = myValuesProductos.cantidadCanastillas;
      var valueUser = int.Parse(fieldCantidadCanastillas2.text);
      if(valueUser >= aux / valueError && valueUser <= aux * valueError)
      {
         if(!duplicateValue3)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue3 = true;
         }
      }
      else
      {
         duplicateValue3 = false;
      }

      Debug.Log("VerificarCantidadCanastillas2 = " + calificacion1);
   }

   public void VerificarCantidadBalanzas()
   {
      var aux = 1f;
      var valueUser = int.Parse(fieldBalanzas.text);
      if(valueUser >= aux / valueError && valueUser <= aux * valueError)
      {
         if(!duplicateValue4)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue4 = true;
         }
      }
      else
      {
         duplicateValue4 = false;
      }

      Debug.Log("VerificarCantidadBalanzas = " + calificacion1);
   }

   public void VerificarJornalesLimpiezaSeleccion()
   {
      cantidadJornalesLimpieza = CalculateJornales(3f);
      cantidadJornalesSeleccion = CalculateJornales(4f);
      if(Math.Abs(int.Parse(fieldsLimpiezaSeleccion[0].captionText.text) - cantidadJornalesLimpieza) < 0.1f && Math.Abs(int.Parse(fieldsLimpiezaSeleccion[1].captionText.text) - cantidadJornalesSeleccion) < 0.1f)
      {
         if(!duplicateValue5)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue5 = true;
         }
      }
      else
      {
         duplicateValue5 = false;
      }

      Debug.Log("VerificarJornalesLimpiezaSeleccion = " + calificacion1);
   }

   public void VerificarJornalesAdministradores()
   {
      cantidadJornalesAdmin = 20;
      cantidadJornalesSecretaria = 20;
      if(int.Parse(fieldAdmin[0].captionText.text) == cantidadJornalesAdmin && int.Parse(fieldAdmin[1].captionText.text) == cantidadJornalesSecretaria)
      {
         if(!duplicateValue6)
         {
            calificacion1 += valueToDivideCalification;
            duplicateValue6 = true;
         }
      }
      else
      {
         duplicateValue6 = false;
      }

      Debug.Log("VerificarJornalesAdministradores = " + calificacion1);
   }

   private float CalculateJornales(float aplications)
   {
      var value =  Mathf.Floor(20f / aplications);
      return value;
   }
}