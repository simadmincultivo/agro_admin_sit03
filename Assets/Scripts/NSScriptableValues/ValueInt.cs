#pragma warning disable 0660
#pragma warning disable 0661
using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Int", menuName = "NSScriptableValue/Int", order = 0)]
    public class ValueInt : AbstractScriptableValue<int>
    {
        #region sobre carga operadores

        public static implicit operator int(ValueInt argValueA)
        {
            return argValueA.value;
        }

        public static ValueInt operator +(ValueInt argValueA, ValueInt argValueB)
        {
            argValueA.Value += argValueB.Value;
            return argValueA;
        }

        public static ValueInt operator -(ValueInt argValueA, ValueInt argValueB)
        {
            argValueA.Value -= argValueB.Value;
            return argValueA;
        }

        public static ValueInt operator *(ValueInt argValueA, int argValueB)
        {
            argValueA.Value *= argValueB;
            return argValueA;
        }

        public static ValueInt operator /(ValueInt argValueA, int argValueB)
        {
            argValueA.Value /= argValueB;
            return argValueA;
        }

        public static ValueInt operator ++(ValueInt argValueA)
        {
            argValueA.Value++;
            return argValueA;
        }

        public static ValueInt operator --(ValueInt argValueA)
        {
            argValueA.Value--;
            return argValueA;
        }

        public static bool operator >(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value > argValueB.value;
        }

        public static bool operator <(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value < argValueB.value;
        }

        public static bool operator ==(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value == argValueB.value;
        }

        public static bool operator !=(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value != argValueB.value;
        }

        public static bool operator >=(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value >= argValueB.value;
        }

        public static bool operator <=(ValueInt argValueA, ValueInt argValueB)
        {
            return argValueA.value <= argValueB.value;
        }

        #endregion
    }
}